/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Blindings are random bytes which conceals the value of a secret share, when hashed alongside the
 * share.
 */
export class Blindings {
  /** Size of the blinding in bytes. */
  static readonly SIZE_OF_BLINDING = 32;

  readonly blindings: Blinding[];

  constructor(blindings: Blinding[]) {
    this.blindings = blindings;
  }

  /**
   * Generates blindings for a number of engines using a random number generator. Each blinding is
   * used to blind the value of one secret share.
   *
   * @param numOfEngines the number of engines to generate blindings for
   * @param rng random number generator used to generate random bytes
   * @return the generated blindings
   */
  static generate(numOfEngines: number, rng: (ln: number) => Buffer): Blindings {
    const generatedBlindings: Blinding[] = [];
    for (let i = 0; i < numOfEngines; i++) {
      generatedBlindings.push(rng(this.SIZE_OF_BLINDING));
    }
    return new Blindings(generatedBlindings);
  }
}

export type Blinding = Buffer;
