package com.partisiablockchain.language.zkclient.node;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.zkclient.ZkContractNode;
import com.secata.stream.SafeDataOutputStream;

/** Send and get secrets from a REAL Node, by sending request directly to the REAL Node. */
public final class RealContractNode implements ZkContractNode {

  /** Header that must be hashed when requesting shares of a secret variable. */
  private static final byte[] VARIABLE_HEADER =
      new byte[] {'s', 'h', 'a', 'm', 'i', 'r', 'v', 'a', 'r', 'i', 'a', 'b', 'l', 'e'};

  private static final String REST_ENDPOINT_STRING_FORMAT = "%s/real/binary/input/%s/%s/%s";
  private final WebClient webClient;
  private final BlockchainAddress contract;
  private final BlockchainAddress address;
  private final BlockchainPublicKey publicKey;
  private final String restInterface;

  /**
   * Create new Real Node to send and fetch secrets, to and from.
   *
   * @param webClient the http client to send requests to the node with.
   * @param contract the deployed real contract the node is allocated to.
   * @param nodeAddress the blockchain address of the node.
   * @param publicKey the public key of the node, used for encrypting messages.
   * @param restInterface the rest end point to send requests to.
   */
  RealContractNode(
      WebClient webClient,
      BlockchainAddress contract,
      BlockchainAddress nodeAddress,
      BlockchainPublicKey publicKey,
      String restInterface) {
    this.webClient = webClient;
    this.contract = contract;
    this.address = nodeAddress;
    this.publicKey = publicKey;
    this.restInterface = restInterface;
  }

  @Override
  public void sendShareToNode(BlockchainAddress account, Hash inputTransaction, byte[] share) {

    InputDto inputDto = new InputDto(share);

    webClient.put(inputUrl(contract, account, inputTransaction), inputDto);
  }

  @Override
  public byte[] fetchShareFromNode(Signature signature, int variableId) {
    OutputDto output =
        webClient.post(
            outputUrl(contract, variableId), SignatureDto.create(signature), OutputDto.class);

    return output.data();
  }

  @Override
  public BlockchainPublicKey getPublicKey() {
    return publicKey;
  }

  @Override
  public String getRestInterface() {
    return restInterface;
  }

  /**
   * Get the blockchain address the node is associated to.
   *
   * @return the blockchain address of the node.
   */
  public BlockchainAddress getAddress() {
    return address;
  }

  private String outputUrl(BlockchainAddress contractAddress, int variableId) {
    return "%s/real/binary/output/variable/%s/%d"
        .formatted(restInterface, contractAddress.writeAsString(), variableId);
  }

  private String inputUrl(
      BlockchainAddress contractAddress, BlockchainAddress accountAddress, Hash clientId) {
    return REST_ENDPOINT_STRING_FORMAT.formatted(
        restInterface,
        contractAddress.writeAsString(),
        accountAddress.writeAsString(),
        clientId.toString());
  }

  @Override
  public Hash hashRequest(int variableId) {
    return Hash.create(
        stream -> {
          stream.write(VARIABLE_HEADER);
          getAddress().write(stream);
          contract.write(stream);
          stream.writeInt(variableId);
        });
  }

  @SuppressWarnings("ArrayRecordComponent")
  record SignatureDto(byte[] signature) {
    static SignatureDto create(Signature signature) {
      return new SignatureDto(SafeDataOutputStream.serialize(signature));
    }
  }

  @SuppressWarnings("ArrayRecordComponent")
  record OutputDto(byte[] data) {}

  @SuppressWarnings("ArrayRecordComponent")
  record InputDto(byte[] data) {}
}
