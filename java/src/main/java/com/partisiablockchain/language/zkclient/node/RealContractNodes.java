package com.partisiablockchain.language.zkclient.node;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.zkclient.ZkContractNode;
import com.partisiablockchain.language.zkclient.ZkContractNodes;
import com.partisiablockchain.language.zkclient.shares.BinarySecretShares;
import com.partisiablockchain.language.zkclient.shares.BlindedBinaryShares;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.List;
import java.util.stream.IntStream;

/**
 * The REAL nodes allocated to a specific Zk Contract, used for sending and fetching secret data to
 * and from the REAL nodes.
 */
public final class RealContractNodes implements ZkContractNodes {

  private final List<ZkContractNode> nodes;
  private final TimeSleeper timeSleeper;

  /**
   * Create a collection of REAL Nodes to send and fetch secrets with.
   *
   * @param nodes the nodes assigned to a specific contract.
   */
  RealContractNodes(List<ZkContractNode> nodes) {
    this(nodes, TimeSleeper.SYSTEM);
  }

  /**
   * Create a collection of REAL Nodes to send and fetch secrets with.
   *
   * @param nodes the nodes assigned to a specific contract.
   * @param timeSleeper support for sleeping
   */
  RealContractNodes(List<ZkContractNode> nodes, TimeSleeper timeSleeper) {
    this.nodes = nodes;
    this.timeSleeper = timeSleeper;
  }

  @Override
  public BinarySecretShares fetchSharesFromNodes(
      SenderAuthentication senderAuthentication, int variableId) {
    List<byte[]> sharesFromNodes =
        nodes.stream()
            .map(
                node -> {
                  Hash hash = node.hashRequest(variableId);
                  Signature signature = senderAuthentication.sign(hash);
                  return node.fetchShareFromNode(signature, variableId);
                })
            .toList();

    return BinarySecretShares.read(sharesFromNodes);
  }

  @Override
  public void sendSharesToNodes(
      Hash transactionHash,
      BlockchainAddress sender,
      BlockchainAddress zkContractAddress,
      BlindedBinaryShares blindedShares) {

    int numberOfRetries = 4;
    boolean[] hasSucceeded = new boolean[nodes.size()];
    for (int i = 0; i < numberOfRetries; i++) {
      if (i != 0) {
        int finalI = i;
        ExceptionConverter.run(() -> timeSleeper.sleep(5000L * finalI));
      }
      for (int j = 0; j < nodes.size(); j++) {
        if (hasSucceeded[j]) {
          continue;
        }
        BlindedBinaryShares.Share share = blindedShares.getShares().get(j);
        try {
          nodes.get(j).sendShareToNode(sender, transactionHash, share.blinded());
          hasSucceeded[j] = true;
        } catch (RuntimeException ignored) {
          // Failed, try again
        }
      }
      if (IntStream.range(0, hasSucceeded.length).allMatch(j -> hasSucceeded[j])) {
        return;
      }
    }
    var numberOfSucceeded =
        IntStream.range(0, hasSucceeded.length).filter(j -> hasSucceeded[j]).count();
    if (numberOfSucceeded < nodes.size() - 1) {
      throw new RuntimeException("Unable to send shares to nodes");
    }
  }

  @Override
  public List<BlockchainPublicKey> getPublicKeysOfNodes() {
    return nodes.stream().map(ZkContractNode::getPublicKey).toList();
  }

  @Override
  public List<String> getRestInterfacesOfNodes() {
    return nodes.stream().map(ZkContractNode::getRestInterface).toList();
  }

  @Override
  public List<Signature> createFetchSignatures(SenderAuthentication owner, int variableId) {
    return nodes.stream().map(node -> owner.sign(node.hashRequest(variableId))).toList();
  }

  /** Extension interface used internally in testing. */
  @FunctionalInterface
  interface TimeSleeper {
    /** Default system sleep. */
    TimeSleeper SYSTEM = Thread::sleep;

    /**
     * Sleep.
     *
     * @param millis duration to sleep for
     */
    void sleep(long millis) throws Exception;
  }
}
