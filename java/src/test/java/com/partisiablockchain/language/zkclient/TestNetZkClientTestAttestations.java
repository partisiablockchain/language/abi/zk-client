package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abicodegen.RealDeploy;
import com.partisiablockchain.language.abicodegen.ZkSecondPrice;
import com.partisiablockchain.language.recordreplay.BlockchainRecordReplay;
import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

final class TestNetZkClientTestAttestations {

  private static final boolean RECORDING = false;
  private static final String REFERENCE_FOLDER = "src/test/resources/zkclient/";
  private static final String TESTNET = "https://node1.testnet.partisiablockchain.com";
  private static final BlockchainAddress REAL_DEPLOY =
      BlockchainAddress.fromString("018bc1ccbb672b87710327713c97d43204905082cb");

  private static BlockchainAddress contractAddress;
  private static RealZkClient zkClient;

  private static final SenderAuthenticationKeyPair OWNER_KEY =
      SenderAuthenticationKeyPair.fromString(
          "6d123bc5ed7632934c9155b8726de858a20e85cbd578e12f2b863d852c04343a");
  private static final SenderAuthenticationKeyPair USER1_KEY =
      SenderAuthenticationKeyPair.fromString(
          "1bfa9425af347e9b2592c3fb608d51a761e44778e9dae276c04a020f1acd704c");
  private static final SenderAuthenticationKeyPair USER2_KEY =
      SenderAuthenticationKeyPair.fromString(
          "e2c8d265e7d7d53f468ec1bf9d5485106a11c6b393e930952964262f2ee00644");
  private static final SenderAuthenticationKeyPair USER3_KEY =
      SenderAuthenticationKeyPair.fromString(
          "2988c872289f3f16b385a6d2de86395a6beac5fbed0ba6aaf4a56dda4865d1f7");
  private static BlockchainClient client;
  private static int secondHighestBid;
  private static int highestBidder;

  /**
   * Setup of second price auction contract ensuring that there is an attestation in the state to
   * fetch.
   */
  @BeforeAll
  static void deploySecondPriceAuctionContract(TestInfo info) throws InterruptedException {

    BlockchainRecordReplay recordReplay =
        new BlockchainRecordReplay(RECORDING, REFERENCE_FOLDER + info.getDisplayName(), TESTNET, 3);

    BlockchainTransactionClient deployClient = recordReplay.getTransactionClient(OWNER_KEY);

    client = recordReplay.getBlockchainClient();

    byte[] secondPriceInit = ZkSecondPrice.initialize();
    byte[] secondPriceAbi =
        ExceptionConverter.call(
            () -> Files.readAllBytes(Path.of("src/test/resources/contract/zk_second_price.abi")));
    byte[] secondPriceZkwa =
        ExceptionConverter.call(
            () -> Files.readAllBytes(Path.of("src/test/resources/contract/zk_second_price.zkwa")));
    byte[] deploymentBytes =
        RealDeploy.deployContractV3(
            secondPriceZkwa, secondPriceInit, secondPriceAbi, 20000000, List.of(), 8);

    Transaction deployTransaction = Transaction.create(REAL_DEPLOY, deploymentBytes);

    SentTransaction sentTransaction = deployClient.signAndSend(deployTransaction, 2200000);
    deployClient.waitForSpawnedEvents(sentTransaction);
    contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_ZK, sentTransaction.signedTransaction().identifier());

    Random rng = new Random(12345L);
    zkClient =
        RealZkClient.createForTest(
            contractAddress, recordReplay.getWebClient(), recordReplay.getBlockchainClient(), rng);

    int lowestBidder = 1;
    highestBidder = 2;
    int secondHighestBidder = 3;

    // Register 3 bidders
    byte[] registerBidder1 = ZkSecondPrice.registerBidder(lowestBidder, USER1_KEY.getAddress());
    byte[] registerBidder2 = ZkSecondPrice.registerBidder(highestBidder, USER2_KEY.getAddress());
    byte[] registerBidder3 =
        ZkSecondPrice.registerBidder(secondHighestBidder, USER3_KEY.getAddress());

    Transaction registerBidder1Transaction = Transaction.create(contractAddress, registerBidder1);
    Transaction registerBidder2Transaction = Transaction.create(contractAddress, registerBidder2);
    Transaction registerBidder3Transaction = Transaction.create(contractAddress, registerBidder3);

    BlockchainTransactionClient owner = recordReplay.getTransactionClient(OWNER_KEY);

    SentTransaction sentTransaction1 = owner.signAndSend(registerBidder1Transaction, 5000);
    owner.waitForSpawnedEvents(sentTransaction1);

    SentTransaction sentTransaction2 = owner.signAndSend(registerBidder2Transaction, 15000);
    owner.waitForSpawnedEvents(sentTransaction2);

    SentTransaction sentTransaction3 = owner.signAndSend(registerBidder3Transaction, 15000);
    owner.waitForSpawnedEvents(sentTransaction3);

    // The bidders bid
    BlockchainTransactionClient user1 = recordReplay.getTransactionClient(USER1_KEY);
    secondHighestBid = 10000;
    Transaction user1BidTransaction =
        zkClient.buildOnChainInputTransaction(
            USER1_KEY.getAddress(), createSecretIntInput(secondHighestBid), addBidPublicRpc());
    SentTransaction onChainInputTransaction1 = user1.signAndSend(user1BidTransaction, 200000);
    user1.waitForSpawnedEvents(onChainInputTransaction1);

    BlockchainTransactionClient user2 = recordReplay.getTransactionClient(USER2_KEY);
    int highestBid = 1034000;
    Transaction user2BidTransaction =
        zkClient.buildOnChainInputTransaction(
            USER2_KEY.getAddress(), createSecretIntInput(highestBid), addBidPublicRpc());
    SentTransaction onChainInputTransaction2 = user2.signAndSend(user2BidTransaction, 200000);
    user2.waitForSpawnedEvents(onChainInputTransaction2);

    BlockchainTransactionClient user3 = recordReplay.getTransactionClient(USER3_KEY);
    int lowestBid = 130;
    Transaction user3BidTransaction =
        zkClient.buildOnChainInputTransaction(
            USER3_KEY.getAddress(), createSecretIntInput(lowestBid), addBidPublicRpc());
    SentTransaction onChainInputTransaction3 = user3.signAndSend(user3BidTransaction, 200000);
    user3.waitForSpawnedEvents(onChainInputTransaction3);

    if (RECORDING) {
      Thread.sleep(15000);
    }

    // The auction owner computes the winner, which attests the result of the auction
    byte[] computeWinnerRpc = ZkSecondPrice.computeWinner();
    Transaction computeWinnerTransaction = Transaction.create(contractAddress, computeWinnerRpc);
    owner.signAndSend(computeWinnerTransaction, 1500000);

    owner.waitForSpawnedEvents(sentTransaction);

    if (RECORDING) {
      Thread.sleep(25000);
    }
  }

  /**
   * A user can fetch an attestation with its attestation id. The attested data and the signatures
   * are correct in the sense that the nodes' addresses and public keys can be recovered, and the
   * signed data corresponds to the deserialized attested data.
   */
  @Test
  void getAttestationSignaturesFromContract() {

    // Use the zk client to get the attestation signatures
    RealZkClient.Attestation attestation =
        zkClient.getAttestationWithSignatures(contractAddress, 1);
    Hash signedData = hashData(attestation.data());

    // Verify that the attestation signatures are valid, i.e. signed by the nodes
    assertThat(attestation.signatures().size()).isEqualTo(4);
    JsonNode engines =
        client.getContractState(contractAddress).serializedContract().get("engines").get("engines");

    for (int i = 0; i < 4; i++) {
      JsonNode node = engines.get(i);
      String address = node.get("identity").textValue();
      String publicKey = node.get("publicKey").textValue();
      Signature signature = attestation.signatures().get(i);
      assertThat(address).isEqualTo(signature.recoverSender(signedData).writeAsString());
      assertThat(publicKey).isEqualTo(signature.recoverPublicKey(signedData).toString());
    }

    // Deserialize the attested data
    ZkSecondPrice.AuctionResult auctionResult = deserializeAuctionResult(attestation.data());
    // The price should be the second-highest bid, and the winner should be the highest bidder
    assertThat(auctionResult.secondHighestBid()).isEqualTo(secondHighestBid);
    assertThat(auctionResult.winner().id()).isEqualTo(highestBidder);
  }

  /** A user cannot fetch an attestation with an id that does not match any attestation id . */
  @Test
  void getAttestationSignaturesWithInvalidId() {
    int invalidId = 10;
    Assertions.assertThatThrownBy(() -> zkClient.getAttestationWithSignatures(contractAddress, 10))
        .hasMessage("Unable to get signatures for the attestation with id " + invalidId);
  }

  private static byte[] addBidPublicRpc() {
    return new byte[] {0x40};
  }

  static CompactBitArray createSecretIntInput(int secret) {
    return BitOutput.serializeBits(output -> output.writeSignedInt(secret, 32));
  }

  private ZkSecondPrice.AuctionResult deserializeAuctionResult(byte[] data) {
    return ZkSecondPrice.deserializeSpecialAuctionResult(data);
  }

  private Hash hashData(byte[] data) {
    return Hash.create(
        stream -> {
          stream.write("ZK_REAL_ATTESTATION".getBytes(StandardCharsets.UTF_8));
          contractAddress.write(stream);
          stream.write(data);
        });
  }
}
