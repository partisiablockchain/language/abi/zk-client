package com.partisiablockchain.language.zkclient.node;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.zkclient.shares.BinarySecretShares;
import com.partisiablockchain.language.zkclient.shares.BlindedBinaryShares;
import com.secata.stream.BitOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class RealContractNodesTest {

  static final Hash TRANSACTION_HASH =
      Hash.create(safeDataOutputStream -> safeDataOutputStream.writeString("Dummy"));
  static final BlockchainAddress SENDER =
      BlockchainAddress.fromString("000000000000000000000000000000000000000000");
  static final BlockchainAddress ZK_CONTRACT =
      BlockchainAddress.fromString("030000000000000000000000000000000000000001");
  static final BlindedBinaryShares BLINDED_SHARES =
      new BinarySecretShares(BitOutput.serializeBits(serializer -> {}), new Random(1234))
          .applyBlinding();

  /** If all nodes succeed immediately then there is no sleeping. */
  @Test
  void sendSharesWithRetriesAllSuccess() {
    List<Long> sleepCalled = new ArrayList<>();
    RealContractNodes realContractNodes =
        new RealContractNodes(
            List.of(
                new ZkContractNodeStub(0),
                new ZkContractNodeStub(0),
                new ZkContractNodeStub(0),
                new ZkContractNodeStub(0)),
            sleepCalled::add);
    realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, ZK_CONTRACT, BLINDED_SHARES);
    Assertions.assertThat(sleepCalled.size()).isEqualTo(0);
  }

  /** If all nodes fail always, then the call fails after maximum sleeping. */
  @Test
  void sendSharesWithRetriesAllFailure() {
    List<Long> sleepCalled = new ArrayList<>();
    RealContractNodes realContractNodes =
        new RealContractNodes(
            List.of(
                new ZkContractNodeStub(10),
                new ZkContractNodeStub(10),
                new ZkContractNodeStub(10),
                new ZkContractNodeStub(10)),
            sleepCalled::add);
    Assertions.assertThatThrownBy(
            () ->
                realContractNodes.sendSharesToNodes(
                    TRANSACTION_HASH, SENDER, ZK_CONTRACT, BLINDED_SHARES))
        .hasMessage("Unable to send shares to nodes");
    Assertions.assertThat(sleepCalled).containsExactly(5000L, 10000L, 15000L);
  }

  /**
   * If a single node fails twice before succeeding, then the call succeeds with two calls to sleep.
   */
  @Test
  void sendSharesWithRetriesSingleNodeFailsTwice() {
    List<Long> sleepCalled = new ArrayList<>();
    RealContractNodes realContractNodes =
        new RealContractNodes(
            List.of(
                new ZkContractNodeStub(0),
                new ZkContractNodeStub(2),
                new ZkContractNodeStub(0),
                new ZkContractNodeStub(0)),
            sleepCalled::add);
    realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, ZK_CONTRACT, BLINDED_SHARES);
    Assertions.assertThat(sleepCalled).containsExactly(5000L, 10000L);
  }

  /** If a single node fails always, then the call still succeeds with maximum sleeping. */
  @Test
  void sendSharesWithRetriesSingleNodeFails() {
    List<Long> sleepCalled = new ArrayList<>();
    RealContractNodes realContractNodes =
        new RealContractNodes(
            List.of(
                new ZkContractNodeStub(0),
                new ZkContractNodeStub(10),
                new ZkContractNodeStub(0),
                new ZkContractNodeStub(0)),
            sleepCalled::add);
    realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, ZK_CONTRACT, BLINDED_SHARES);
    Assertions.assertThat(sleepCalled).containsExactly(5000L, 10000L, 15000L);
  }

  /** If two nodes fails always, then the call fails with maximum sleeping. */
  @Test
  void sendSharesWithRetriesTwoNodesFail() {
    List<Long> sleepCalled = new ArrayList<>();
    RealContractNodes realContractNodes =
        new RealContractNodes(
            List.of(
                new ZkContractNodeStub(0),
                new ZkContractNodeStub(10),
                new ZkContractNodeStub(0),
                new ZkContractNodeStub(10)),
            sleepCalled::add);
    Assertions.assertThatThrownBy(
            () ->
                realContractNodes.sendSharesToNodes(
                    TRANSACTION_HASH, SENDER, ZK_CONTRACT, BLINDED_SHARES))
        .hasMessage("Unable to send shares to nodes");
    Assertions.assertThat(sleepCalled).containsExactly(5000L, 10000L, 15000L);
  }
}
