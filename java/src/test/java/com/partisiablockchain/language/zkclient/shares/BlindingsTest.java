package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.security.SecureRandom;
import java.util.Random;
import org.junit.jupiter.api.Test;

/** Tests for Blindings. */
public final class BlindingsTest {
  @Test
  void testDifferentNumbersOfEngines() {
    Random rng = new SecureRandom();

    Blindings blindings1 = Blindings.generate(3, rng);
    int totalBitLength1 =
        blindings1.getBlindings().stream()
            .map(b -> b.blindingBytes().length)
            .reduce(0, Integer::sum);
    int expectedLength1 = Blindings.SIZE_OF_BLINDING * 3;
    assertThat(totalBitLength1).isEqualTo(expectedLength1);

    Blindings blindings2 = Blindings.generate(4, rng);
    int totalBitLength2 =
        blindings2.getBlindings().stream()
            .map(b -> b.blindingBytes().length)
            .reduce(0, Integer::sum);
    int expectedLength2 = Blindings.SIZE_OF_BLINDING * 4;
    assertThat(totalBitLength2).isEqualTo(expectedLength2);
  }

  @Test
  void sameBlindingsForSameDeterministicInput() {
    int amount = 4;
    int seed = 12345;

    Blindings blindings1 = Blindings.generate(amount, new Random(seed));
    Blindings blindings2 = Blindings.generate(amount, new Random(seed));

    assertThat(blindings1).usingRecursiveComparison().isEqualTo(blindings2);
  }

  @Test
  void differentBlindingsForNondeterministicRng() {
    int amount = 4;

    Blindings blindings1 = Blindings.generate(amount, new SecureRandom());
    Blindings blindings2 = Blindings.generate(amount, new SecureRandom());
    assertThat(blindings1).usingRecursiveComparison().isNotEqualTo(blindings2);
  }
}
