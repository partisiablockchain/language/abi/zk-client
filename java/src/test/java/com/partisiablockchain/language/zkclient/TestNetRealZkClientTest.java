package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.abicodegen.AverageSalary;
import com.partisiablockchain.language.abicodegen.RealDeploy;
import com.partisiablockchain.language.recordreplay.BlockchainRecordReplay;
import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

final class TestNetRealZkClientTest {

  private static final boolean RECORDING = false;
  private static final String REFERENCE_FOLDER = "src/test/resources/zkclient/";
  private static final String TESTNET = "https://node1.testnet.partisiablockchain.com";
  private static final BlockchainAddress REAL_DEPLOY =
      BlockchainAddress.fromString("018bc1ccbb672b87710327713c97d43204905082cb");

  private static final SenderAuthenticationKeyPair OWNER_KEY =
      SenderAuthenticationKeyPair.fromString(
          "6d123bc5ed7632934c9155b8726de858a20e85cbd578e12f2b863d852c04343a");
  private static final SenderAuthenticationKeyPair USER1_KEY =
      SenderAuthenticationKeyPair.fromString(
          "1bfa9425af347e9b2592c3fb608d51a761e44778e9dae276c04a020f1acd704c");
  private static final SenderAuthenticationKeyPair USER2_KEY =
      SenderAuthenticationKeyPair.fromString(
          "e2c8d265e7d7d53f468ec1bf9d5485106a11c6b393e930952964262f2ee00644");
  private static final SenderAuthenticationKeyPair USER3_KEY =
      SenderAuthenticationKeyPair.fromString(
          "2988c872289f3f16b385a6d2de86395a6beac5fbed0ba6aaf4a56dda4865d1f7");
  private static BlockchainTransactionClient OWNER;
  private static BlockchainTransactionClient USER1;
  private static BlockchainTransactionClient USER2;
  private static BlockchainTransactionClient USER3;
  private static BlockchainAddress contractAddress;
  private static RealZkClient zkClient;
  private BlockchainClient client;

  @BeforeAll
  static void deployZk(TestInfo info) {

    BlockchainRecordReplay recordReplay =
        new BlockchainRecordReplay(RECORDING, REFERENCE_FOLDER + info.getDisplayName(), TESTNET, 3);

    BlockchainTransactionClient deployClient = recordReplay.getTransactionClient(OWNER_KEY);

    byte[] initAverageSalary = AverageSalary.initialize();
    byte[] averageSalaryAbi =
        ExceptionConverter.call(
            () -> Files.readAllBytes(Path.of("src/test/resources/contract/average_salary.abi")));
    byte[] averageSalaryZkwa =
        ExceptionConverter.call(
            () -> Files.readAllBytes(Path.of("src/test/resources/contract/average_salary.zkwa")));
    byte[] deploymentBytes =
        RealDeploy.deployContractV3(
            averageSalaryZkwa, initAverageSalary, averageSalaryAbi, 20000000, List.of(), 6);

    Transaction deployTransaction = Transaction.create(REAL_DEPLOY, deploymentBytes);

    SentTransaction sentTransaction = deployClient.signAndSend(deployTransaction, 2000000);
    deployClient.waitForSpawnedEvents(sentTransaction);
    contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_ZK, sentTransaction.signedTransaction().identifier());
  }

  @BeforeEach
  void setup(TestInfo info) {
    BlockchainRecordReplay recordReplay =
        new BlockchainRecordReplay(RECORDING, REFERENCE_FOLDER + info.getDisplayName(), TESTNET, 3);

    Random rng = new Random(1234L);
    zkClient =
        RealZkClient.createForTest(
            contractAddress, recordReplay.getWebClient(), recordReplay.getBlockchainClient(), rng);
    client = recordReplay.getBlockchainClient();

    OWNER = recordReplay.getTransactionClient(OWNER_KEY);
    USER1 = recordReplay.getTransactionClient(USER1_KEY);
    USER2 = recordReplay.getTransactionClient(USER2_KEY);
    USER3 = recordReplay.getTransactionClient(USER3_KEY);
  }

  /** Sending a secret input to a Zk contract on-chain. */
  @Test
  void sendSecretInputOnChain() throws InterruptedException {

    Transaction transaction =
        zkClient.buildOnChainInputTransaction(
            OWNER_KEY.getAddress(), createSecretIntInput(10000), averageSalaryPublicRpc());
    SentTransaction onChainInputTransaction = OWNER.signAndSend(transaction, 20000);
    OWNER.waitForSpawnedEvents(onChainInputTransaction);

    if (RECORDING) {
      Thread.sleep(10000);
    }
    JsonNode state = client.getContractState(contractAddress).serializedContract();
    Hash identifier = onChainInputTransaction.signedTransaction().identifier();
    Assertions.assertThat(isInputReceived(state, identifier)).isTrue();
  }

  /** Sending a secret input to a Zk contract off-chain. */
  @Test
  void sendSecretInputOffChain() throws InterruptedException {

    RealZkClient.OffChainInput offChainInput =
        zkClient.buildOffChainInputTransaction(
            createSecretIntInput(10000), averageSalaryPublicRpc());
    SentTransaction offChainInputTransaction =
        USER1.signAndSend(offChainInput.transaction(), 20000);
    zkClient.sendOffChainInputToNodes(
        contractAddress,
        USER1_KEY.getAddress(),
        offChainInputTransaction.signedTransaction().identifier(),
        offChainInput.shares());

    USER1.waitForSpawnedEvents(offChainInputTransaction);

    if (RECORDING) {
      Thread.sleep(10000);
    }
    JsonNode state = client.getContractState(contractAddress).serializedContract();
    Hash identifier = offChainInputTransaction.signedTransaction().identifier();
    Assertions.assertThat(isInputReceived(state, identifier)).isTrue();
  }

  /** Fetch a secret input from the zk nodes. */
  @Test
  void fetchSecretFromNodes() {

    Transaction transaction =
        zkClient.buildOnChainInputTransaction(
            USER2_KEY.getAddress(), createSecretIntInput(10000), averageSalaryPublicRpc());
    SentTransaction sentTransaction = USER2.signAndSend(transaction, 20000);

    USER2.waitForSpawnedEvents(sentTransaction);

    ExceptionConverter.run(() -> Thread.sleep(10000));

    CompactBitArray compactBitArray = zkClient.fetchSecretVariable(USER2_KEY, 3);
    Assertions.assertThat(compactBitArray.data()).isNotEmpty();
  }

  /** A secret cannot be fetched by a user that does not own the secret variable. */
  @Test
  void secretFetchedByAnotherAccountThanTheOwner() {

    Transaction transaction =
        zkClient.buildOnChainInputTransaction(
            USER3_KEY.getAddress(), createSecretIntInput(10000), averageSalaryPublicRpc());
    SentTransaction sentTransaction = USER3.signAndSend(transaction, 20000);

    USER3.waitForSpawnedEvents(sentTransaction);

    ExceptionConverter.run(() -> Thread.sleep(10000));

    Assertions.assertThatThrownBy(() -> zkClient.fetchSecretVariable(USER1_KEY, 1))
        .hasMessage("HTTP 404 Not Found");
  }

  static boolean isInputReceived(JsonNode state, Hash transactionHash) {
    JsonNode variables = state.get("variables");
    for (JsonNode variable : variables) {
      if (transactionHash.equals(
          Hash.fromString(variable.get("value").get("transaction").textValue()))) {
        return true;
      }
    }
    return false;
  }

  private byte[] averageSalaryPublicRpc() {
    return new byte[] {0x40};
  }

  CompactBitArray createSecretIntInput(int secret) {
    return BitOutput.serializeBits(output -> output.writeSignedInt(secret, 32));
  }
}
