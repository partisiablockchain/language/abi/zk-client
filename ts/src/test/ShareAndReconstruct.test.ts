/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BitInput, BitOutput } from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { BinarySecretShares } from "../main";

test("share and reconstruct number", () => {
  const secret = 55;

  const secretVarData = BitOutput.serializeBits((out) => out.writeSignedNumber(secret, 32));
  const secretShares = BinarySecretShares.create(secretVarData);

  const reconstructedSecret = secretShares.reconstructSecret();
  const converted = new BitInput(reconstructedSecret.data).readSignedNumber(32);
  expect(converted).toEqual(secret);
});

test("share and reconstruct BN", () => {
  const secret = new BN(123456789);

  const secretVarData = BitOutput.serializeBits((out) => out.writeSignedBN(secret, 64));
  const secretShares = BinarySecretShares.create(secretVarData);

  const reconstructedSecret = secretShares.reconstructSecret();
  const converted = new BitInput(reconstructedSecret.data).readSignedBN(64);
  expect(converted.eq(secret)).toEqual(true);
});
