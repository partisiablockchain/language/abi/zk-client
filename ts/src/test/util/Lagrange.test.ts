/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BinaryExtensionFieldElement } from "../../main";
import { Lagrange } from "../../main";
import { Polynomial } from "../../main";

describe("Lagrange tests", () => {
  const xs = [
    BinaryExtensionFieldElement.createElement(0),
    BinaryExtensionFieldElement.createElement(3),
    BinaryExtensionFieldElement.createElement(6),
  ];
  const ys = [
    BinaryExtensionFieldElement.createElement(2),
    BinaryExtensionFieldElement.createElement(11),
    BinaryExtensionFieldElement.createElement(38),
  ];

  const evalPoly = (poly: Polynomial<BinaryExtensionFieldElement>, x: number): number => {
    return poly.evaluate(BinaryExtensionFieldElement.createElement(x)).value;
  };

  const asNumber = (left: BinaryExtensionFieldElement[]): number[] => left.map((e) => e.value);

  test("coefficients", () => {
    const polynomial = Lagrange.interpolate(
      xs,
      ys,
      BinaryExtensionFieldElement.ZERO,
      BinaryExtensionFieldElement.ONE
    );

    expect(polynomial.degree()).toBe(2);
    expect(asNumber(polynomial.getCoefficients())).toEqual([2, 14, 7]);

    expect(evalPoly(polynomial, 4)).toBe(6);
    expect(evalPoly(polynomial, 13)).toBe(4);
    expect(evalPoly(polynomial, 17)).toBe(11);
  });

  test("Different length xs and ys", () => {
    const caller = () => {
      Lagrange.interpolateCheckDegree(
        xs,
        [],
        1,
        BinaryExtensionFieldElement.ZERO,
        BinaryExtensionFieldElement.ONE
      );
    };
    expect(caller).toThrowError("xs and ys must be of same size");
  });

  test("zero length xs and ys", () => {
    const caller = () => {
      Lagrange.interpolateCheckDegree(
        [],
        [],
        1,
        BinaryExtensionFieldElement.ZERO,
        BinaryExtensionFieldElement.ONE
      );
    };
    expect(caller).toThrowError("xs and ys must have at least one element");
  });

  test("maximal degree correct", () => {
    const poly = Lagrange.interpolateCheckDegree(
      xs,
      ys,
      2,
      BinaryExtensionFieldElement.ZERO,
      BinaryExtensionFieldElement.ONE
    );
    expect(poly.degree()).toBe(2);
  });

  test("actual degree less than max", () => {
    const poly = Lagrange.interpolateCheckDegree(
      xs,
      ys,
      3,
      BinaryExtensionFieldElement.ZERO,
      BinaryExtensionFieldElement.ONE
    );
    expect(poly.degree()).toBe(2);
  });

  test("maximal degree wrong", () => {
    const caller = () => {
      Lagrange.interpolateCheckDegree(
        xs,
        ys,
        1,
        BinaryExtensionFieldElement.ZERO,
        BinaryExtensionFieldElement.ONE
      );
    };
    expect(caller).toThrowError(
      "Interpolated polynomial has too high degree. Expected maximal=1, actual=2"
    );
  });
});
