package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Blindings are random bytes which conceals the value of a secret share, when hashed alongside the
 * share.
 */
public final class Blindings {
  /** Size of the blinding in bytes. */
  static final int SIZE_OF_BLINDING = 32;

  private final List<Blinding> blindings;

  private Blindings(List<Blinding> blindings) {
    this.blindings = blindings;
  }

  /**
   * Generates blindings for a number of engines using a random number generator. Each blinding is
   * used to blind the value of one secret share.
   *
   * @param numOfEngines the number of engines to generate blindings for
   * @param rng random number generator used to generate random bytes
   * @return the generated blindings
   */
  public static Blindings generate(int numOfEngines, Random rng) {
    List<Blinding> generatedBlindings = new ArrayList<>();
    for (int i = 0; i < numOfEngines; i++) {
      byte[] blinding = new byte[SIZE_OF_BLINDING];
      rng.nextBytes(blinding);
      generatedBlindings.add(new Blinding(blinding));
    }

    return new Blindings(generatedBlindings);
  }

  /**
   * Returns the list of blindings.
   *
   * @return the list
   */
  public List<Blinding> getBlindings() {
    return blindings;
  }

  /**
   * A blinding, which is an array of random bytes.
   *
   * @param blindingBytes the bytes composing the blinding
   */
  @SuppressWarnings("ArrayRecordComponent")
  record Blinding(byte[] blindingBytes) {}
}
