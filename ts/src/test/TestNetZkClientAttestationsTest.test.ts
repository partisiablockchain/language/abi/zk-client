/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import fetchMock from "jest-fetch-mock";
import { RealZkClient } from "../main";
import { Client } from "../main";
// eslint-disable-next-line import/no-internal-modules
import * as getAttestationsJson from "./json-request-response/getAttestations.json";
// eslint-disable-next-line import/no-internal-modules
import * as getAttestationsInvalidIdJson from "./json-request-response/getAttestationsInvalidId.json";
import { Attestation } from "../main/RealZkClient";

/** Address of the testnet node to request shares from. */
const TESTNET_URL = "https://node1.testnet.partisiablockchain.com";

/** Address of the second price auction zk-contract. */
const ZK_CONTRACT = "03159f37b8fc92650fdc4120784a7f7263d0a21f48";

describe("zk client - contract with attestations", () => {
  let realZkClient: RealZkClient;

  /** Create a RealZkClient by mocking the serialized contract. */
  beforeEach(async () => {
    const client = new Client(TESTNET_URL);
    fetchMock.resetMocks();
    fetchMock.mockResponseOnce(
      JSON.stringify({
        serializedContract: getAttestationsJson["0"].response.prettyEntity.serializedContract,
      })
    );
    realZkClient = await RealZkClient.createForTest(ZK_CONTRACT, client, () =>
      Buffer.from("this is random")
    );
  });

  /** Get the attestation signatures using the zkClient. */
  test("get attestations", async () => {
    fetchMock.resetMocks();
    // The response should be the serialized contract containing attestation signatures
    fetchMock.mockResponseOnce(JSON.stringify(getAttestationsJson["65"].response.prettyEntity));

    const attestationWithSignatures: Attestation = await realZkClient.getAttestationWithSignatures(
      ZK_CONTRACT,
      1
    );

    // Verify that the call is a GET request to get the contract state
    const call = fetchMock.mock.calls;
    expect(call["0"]["1"]?.method).toEqual(getAttestationsJson["65"].request.httpMethod);
    expect(call["0"]["0"]?.toString().slice(0, 123)).toEqual(
      getAttestationsJson["65"].request.url.slice(0, 123)
    );

    // Expect to get 4 attestation signatures, one from each node
    expect(attestationWithSignatures.signatures.length).toBe(4);
    const expectedData =
      getAttestationsJson["65"].response.prettyEntity.serializedContract.attestations[0].value.data
        .data;
    expect(attestationWithSignatures.data.toString("base64")).toEqual(expectedData);
  });

  /** A user can only get signatures of an existing attestation,
   * i.e. one with an existing attestation id. */
  test("get non-existent attestation signatures", async () => {
    fetchMock.resetMocks();
    fetchMock.mockResponseOnce(
      JSON.stringify(getAttestationsInvalidIdJson["27"].response.prettyEntity)
    );
    const invalidId = 10;
    await expect(() =>
      realZkClient.getAttestationWithSignatures(ZK_CONTRACT, invalidId)
    ).rejects.toThrow("Unable to get signatures for the attestation with id " + invalidId);

    const call = fetchMock.mock.calls;
    expect(call["0"]["1"]?.method).toEqual(getAttestationsInvalidIdJson["27"].request.httpMethod);
    expect(call["0"]["0"]?.toString().slice(0, 123)).toEqual(
      getAttestationsInvalidIdJson["27"].request.url.slice(0, 123)
    );
  });

  test("fail to get contract data", async () => {
    fetchMock.resetMocks();
    const contractAddress = "000000000000000000000000000000000000000000";
    await expect(
      realZkClient.getAttestationWithSignatures(contractAddress, 1)
    ).rejects.toThrowError("Couldn't get contract data");
  });
});
