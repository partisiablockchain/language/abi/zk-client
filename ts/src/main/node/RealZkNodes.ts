/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ZkNodes } from "./ZkNodes";
import { Client, Engine, ZkContract } from "../util/Client";
import { BlockchainAddress } from "@partisiablockchain/blockchain-api-transaction-client";
import { ZkContractNodes } from "./ZkContractNodes";
import { ZkContractNode } from "./ZkContractNode";
import { RealContractNode } from "./RealContractNode";
import { RealContractNodes } from "./RealContractNodes";

/** Get node information about the REAL Nodes associated with a contract. */
export class RealZkNodes implements ZkNodes {
  readonly blockchainClient: Client;

  constructor(blockchainClient: Client) {
    this.blockchainClient = blockchainClient;
  }

  public static create(blockchainClient: Client) {
    return new RealZkNodes(blockchainClient);
  }

  /**
   * Create the RealZkNodes used for communicating with the nodes and chain when sending inputs and
   * getting secrets from the nodes.
   *
   * @param contractAddress address of the zk contract.
   * @return the nodes to send interactions with.
   */
  async getNodes(contractAddress: BlockchainAddress): Promise<ZkContractNodes> {
    const zkContract: ZkContract | undefined =
      await this.blockchainClient.getContractState(contractAddress);

    if (zkContract === undefined) {
      throw new Error("Couldn't get contract data");
    }
    const nodelist: Engine[] = zkContract.serializedContract.engines.engines;

    const nodes = new Array<ZkContractNode>();

    for (let i = 0; i < nodelist.length; i++) {
      const engineInformation: Engine = nodelist[i];

      const identity: string = engineInformation.identity;
      const publicKey: string = engineInformation.publicKey;
      const restInterface: string = engineInformation.restInterface;

      nodes.push(new RealContractNode(contractAddress, identity, publicKey, restInterface));
    }

    return new RealContractNodes(nodes);
  }
}
