/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainAddress } from "@partisiablockchain/blockchain-api-transaction-client";
import { BlockchainPublicKey } from "../util/BlockchainPublicKey";

/** A single computation engine running a ZK computation. */
export interface ZkContractNode {
  /**
   * Send a secret share to this node.
   *
   * @param account the input sender.
   * @param inputTransaction pointer to on-chain transaction, used for authentication of the share.
   * @param share the first share for this engine.
   */
  sendShareToNode(
    account: BlockchainAddress,
    inputTransaction: string,
    share: Buffer
  ): Promise<boolean>;

  /**
   * Get the share held by the Zk Node for a given variable, only the owner of the variable is
   * allowed to fetch the variable.
   *
   * @param signedMessage the signature of the fetch request.
   * @param variableId the id of variable.
   * @return the bytes of the share.
   */
  fetchSharesFromNode(signedMessage: Buffer, variableId: number): Promise<Buffer>;

  /**
   * Get the public key for the node.
   *
   * @return the public key of the node.
   */
  getPublicKey(): BlockchainPublicKey;

  createMessage(variableId: number): Buffer;
}
