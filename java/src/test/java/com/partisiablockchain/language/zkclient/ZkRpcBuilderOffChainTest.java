package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.zkclient.shares.BinarySecretShares;
import com.partisiablockchain.language.zkclient.shares.BlindedBinaryShares;
import com.partisiablockchain.language.zkclient.shares.ShareCommitments;
import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import com.secata.stream.SafeDataInputStream;
import java.util.List;
import java.util.Random;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Tests off-chain methods of ZkRpcBuilder. */
public final class ZkRpcBuilderOffChainTest {
  /// Parameters used to generate the reference rpc
  private static final int REFERENCE_SECRET = 13;
  private static final byte REFERENCE_INVOCATION = 0x40;
  private static final long REFERENCE_SEED = 12345;

  private byte[] referenceRpc;
  private CompactBitArray referenceInput;
  private byte[] referenceAdditionalRpc;
  private BinarySecretShares referenceSecretShares;

  @BeforeEach
  void referenceConstants() {
    referenceRpc =
        Hex.decode(
            "040000000100000020957976d3afcf19d3a6d011b059400830"
                + "1342927f849ac5194b96ded06a93936028986bfc82b5a5c4ed"
                + "48ea3194139a950aa763b836f02b7b75e9877fb06c5457487e"
                + "78d895d0e6f5fc46616050d8237c42e5bd53464d192d5f00ae"
                + "29ba39a6f90e683b8294bdb3115aca142c94d16f876924939f"
                + "4f703616645e0d5a3835e99a40");

    referenceInput = BitOutput.serializeBits(s -> s.writeSignedInt(REFERENCE_SECRET, 32));

    referenceAdditionalRpc = new byte[] {REFERENCE_INVOCATION};

    referenceSecretShares = new BinarySecretShares(referenceInput, referenceRng());
  }

  private Random referenceRng() {
    return new Random(REFERENCE_SEED);
  }

  /** Test that a new rpc matches a reference rpc that has been tested on the testnet. */
  @Test
  public void rpcMatchesReference() {
    byte[] generatedRpc =
        ZkRpcBuilder.zkInputOffChain(referenceSecretShares, referenceAdditionalRpc).rpc();

    assertThat(generatedRpc).isEqualTo(referenceRpc);
  }

  /** Test that varying the parameters of the rpc builder generates different rpcs. */
  @Test
  void differentRpcForDifferentInput() {
    byte[] someZkRpc =
        ZkRpcBuilder.zkInputOffChain(referenceSecretShares, referenceAdditionalRpc).rpc();

    int newInt = 41;
    CompactBitArray differentInput = BitOutput.serializeBits(s -> s.writeSignedInt(newInt, 32));
    BinarySecretShares differentInputShares =
        new BinarySecretShares(differentInput, referenceRng());
    byte[] withDifferentSecret =
        ZkRpcBuilder.zkInputOffChain(differentInputShares, referenceAdditionalRpc).rpc();
    assertThat(someZkRpc).isNotEqualTo(withDifferentSecret);

    byte differentShortname = 0x41;
    byte[] differentAdditionalRpc = new byte[] {differentShortname};

    byte[] withDifferentInvocation =
        ZkRpcBuilder.zkInputOffChain(referenceSecretShares, differentAdditionalRpc).rpc();
    assertThat(someZkRpc).isNotEqualTo(withDifferentInvocation);
  }

  @Test
  void testBitLengths() {
    ShareCommitments commitments = referenceSecretShares.applyBlinding().hash();
    byte[] serializedCommitments = commitments.serialize();
    int expectedBitLength = Hash.BYTES * referenceSecretShares.noOfShares();
    assertThat(serializedCommitments.length).isEqualTo(expectedBitLength);
  }

  /** Tests that the blinded shares sent to ZK nodes hashes to the same value as in the rpc. */
  @Test
  void testBlindedSharesAgainstPayload() {
    ZkRpcBuilder.OffChainInput offChainInput =
        ZkRpcBuilder.zkInputOffChain(referenceSecretShares, referenceAdditionalRpc);

    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(offChainInput.rpc());

    // read and discard irrelevant bytes
    inputStream.readUnsignedByte(); // read invocation
    int bitLengths = inputStream.readInt();
    for (int i = 0; i < bitLengths; i++) {
      inputStream.readInt(); // read bit length
    }

    List<BlindedBinaryShares.Share> blindedShares = offChainInput.blindedBinaryShares().getShares();
    assertThat(blindedShares).hasSize(4);
    for (BlindedBinaryShares.Share share : blindedShares) {
      Hash fromStream = Hash.read(inputStream);
      Hash fromBlindedShares = Hash.create(s -> s.write(share.blinded()));
      assertThat(fromStream).isEqualTo(fromBlindedShares);
    }
    // last byte should be invocation identifier
    assertThat(inputStream.readUnsignedByte()).isEqualTo(REFERENCE_INVOCATION);
  }
}
