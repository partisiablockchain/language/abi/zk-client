/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BinarySecretShares, CryptoUtils } from "..";
import { BlindedBinaryShares } from "../BlindedBinaryShares";
import { BlockchainAddress } from "@partisiablockchain/blockchain-api-transaction-client";
import { BlockchainPublicKey } from "../util/BlockchainPublicKey";
import { ZkContractNodes } from "./ZkContractNodes";
import { ZkContractNode } from "./ZkContractNode";
import { SignatureProvider } from "../util/SignatureProvider";

export class RealContractNodes implements ZkContractNodes {
  readonly nodes: ZkContractNode[];
  readonly timeSleeper: TimeSleeper;
  readonly numberOfSends: number;

  constructor(nodes: ZkContractNode[], timeSleeper?: TimeSleeper, numberOfSends?: number) {
    this.nodes = nodes;
    this.timeSleeper = timeSleeper ?? new TimeSleeper(timeout);
    this.numberOfSends = numberOfSends ?? 4;
  }

  async fetchSharesFromNodes(
    owner: SignatureProvider,
    variableId: number
  ): Promise<BinarySecretShares> {
    const sharesFromNodes = await Promise.all(
      this.nodes.map(async (node) => {
        const messageToBeSigned = node.createMessage(variableId);
        const signedMessage = await owner.sign(CryptoUtils.hashBuffer(messageToBeSigned));
        return node.fetchSharesFromNode(signedMessage, variableId);
      })
    );

    return BinarySecretShares.read(sharesFromNodes);
  }

  async sendSharesToNodes(
    transactionHash: string,
    sender: BlockchainAddress,
    blindedShares: BlindedBinaryShares
  ): Promise<void> {
    const hasSucceeded: boolean[] = [false, false, false, false];

    for (let i = 0; i < this.numberOfSends; i++) {
      if (i != 0) {
        await this.timeSleeper.sleep(5000 * i);
      }

      await Promise.all(
        this.nodes.map(async (node, index) => {
          if (!hasSucceeded[index]) {
            await node
              .sendShareToNode(sender, transactionHash, blindedShares.shares[index])
              .then((bool) => (hasSucceeded[index] = bool))
              // eslint-disable-next-line @typescript-eslint/no-empty-function
              .catch(() => {});
          }
        })
      );

      if (!hasSucceeded.includes(false)) {
        return;
      }
    }
    const numberOfSucceeded = hasSucceeded.filter((p) => p).length;
    if (numberOfSucceeded < this.nodes.length - 1) {
      throw Error("Unable to send shares to nodes");
    }
  }
  getPublicKeysOfNodes(): BlockchainPublicKey[] {
    return this.nodes.map((node) => node.getPublicKey());
  }
}

export async function timeout(msec: number): Promise<void> {
  return new Promise((f) => setTimeout(f, msec));
}

export class TimeSleeper {
  readonly sleepFun: (n: number) => Promise<void>;

  constructor(sleepFun: (n: number) => Promise<void>) {
    this.sleepFun = sleepFun;
  }

  /**
   * Sleep.
   *
   * @param millis duration to sleep for
   */
  async sleep(millis: number): Promise<void> {
    await this.sleepFun(millis);
  }
}
