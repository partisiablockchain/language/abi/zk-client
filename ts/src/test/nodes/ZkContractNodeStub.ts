/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ZkContractNode } from "../../main/node/ZkContractNode";
import { BlockchainPublicKey } from "../../main";
import { BlockchainAddress } from "@partisiablockchain/blockchain-api-transaction-client";

export class ZkContractNodeStub implements ZkContractNode {
  numberOfFailures: number;
  private _sendCounter: number;

  constructor(numberOfFailures: number) {
    this.numberOfFailures = numberOfFailures;
    this._sendCounter = 0;
  }

  getSendCount(): number {
    return this._sendCounter;
  }

  getPublicKey(): BlockchainPublicKey {
    return BlockchainPublicKey.fromString("00".repeat(21).toString());
  }

  sendShareToNode(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    account: BlockchainAddress,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    inputTransaction: string,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    share: Buffer
  ): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this._sendCounter += 1;
      if (this.numberOfFailures > 0) {
        this.numberOfFailures--;
        reject("Failed to send share to node.");
      }
      resolve(true);
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  createMessage(variableId: number): Buffer {
    return Buffer.from("testMessage");
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  fetchSharesFromNode(signedMessage: Buffer, variableId: number): Promise<Buffer> {
    return new Promise(() => Buffer.from("testBuffer"));
  }
}
