package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests for BinarySecretShares. */
public final class BinarySecretSharesTest {

  @Test
  void incorrectNumberOfShares() {
    List<BlockchainPublicKey> tooManyEngines =
        List.of(
            new KeyPair().getPublic(),
            new KeyPair().getPublic(),
            new KeyPair().getPublic(),
            new KeyPair().getPublic(),
            new KeyPair().getPublic());

    CompactBitArray binaryInput = secretVarDataFromInt(5);
    BinarySecretShares shares = new BinarySecretShares(binaryInput);
    Assertions.assertThatThrownBy(
            () ->
                shares.encrypt(
                    new KeyPair(), new KeyPair().getPublic().createAddress(), tooManyEngines))
        .hasMessage("Number of engines (%d) did not match number of shares (%d)".formatted(5, 4));
  }

  @Test
  void readFromStream() {
    byte[] share =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeInt(2); // number of elements
              stream.writeInt(1); // bit length of first element
              stream.write(new byte[] {10}); // shares of first element
              stream.writeInt(2); // bit length of second element
              stream.write(new byte[] {-11, 12}); // shares of second element
            });

    List<byte[]> shares = List.of(share.clone(), share.clone(), share.clone(), share.clone());
    Random rng = new Random(10);
    BinarySecretShares readShares = BinarySecretShares.read(shares);

    BinarySecretShares expectedShares =
        new BinarySecretShares(
            List.of(
                shareFromBytes(new byte[] {10, -11, 12}),
                shareFromBytes(new byte[] {10, -11, 12}),
                shareFromBytes(new byte[] {10, -11, 12}),
                shareFromBytes(new byte[] {10, -11, 12})),
            rng);

    assertThat(readShares.secretShares)
        .usingRecursiveComparison()
        .isEqualTo(expectedShares.secretShares);
  }

  /**
   * Tests that shares are encrypted correctly by decrypting them in the same fashion they are
   * decrypted at the real nodes. <a
   * href="https://gitlab.com/partisiablockchain/real/real-node/-/blob/main/src/main/java/com/partisiablockchain/zk/real/node/online/ZeroKnowledgeVariableHandler.java">Link
   * to the ZeroKnowledgeVariableHandler</a> in real-node where the decryption happens.
   */
  @Test
  void encryptAndDecryptShares() {

    Random rng = new Random(1234L);
    List<BinaryShare> shares =
        List.of(
            shareFromBytes(new byte[] {1, 2}),
            shareFromBytes(new byte[] {3, 4}),
            shareFromBytes(new byte[] {5, 6}),
            shareFromBytes(new byte[] {7, 8}));
    BinarySecretShares secretShares = new BinarySecretShares(shares, rng);
    KeyPair ephemeralKey = new KeyPair();
    List<KeyPair> engineKeys = List.of(new KeyPair(), new KeyPair(), new KeyPair(), new KeyPair());
    List<BlockchainPublicKey> enginePublicKeys =
        engineKeys.stream().map(KeyPair::getPublic).toList();

    BlockchainAddress senderAddress = new KeyPair().getPublic().createAddress();

    // The shares are encrypted using the private part of the ephemeral key and the public part of
    // the engine keys
    EncryptedBinaryShares encryptedShares =
        secretShares.encrypt(ephemeralKey, senderAddress, enginePublicKeys);

    // Iterate through the engines and shares
    for (int i = 0; i < 4; i++) {
      // The shares are decrypted using the public part of the ephemeral key and the private part of
      // the engine keys
      Hash sharedKey = ephemeralKey.getPublic().deriveSharedKey(engineKeys.get(i).getPrivateKey());
      Cipher aes = createDecryptionCipher(sharedKey);
      byte[] shareToDecrypt = encryptedShares.getShares().get(i).encrypted();
      byte[] decryptedBytes = ExceptionConverter.call(() -> aes.doFinal(shareToDecrypt));

      SafeDataInputStream decryptedBytesStream =
          SafeDataInputStream.createFromBytes(decryptedBytes);

      // The first two bytes of the decrypted bytes are the actual share value
      byte[] rawShare = decryptedBytesStream.readBytes(2);
      BinaryShare decryptedShare = shareFromBytes(rawShare);
      assertThat(decryptedShare).usingRecursiveComparison().isEqualTo(shares.get(i));

      // The rest of the decrypted bytes contain the sender address
      BlockchainAddress decryptedAddress = BlockchainAddress.read(decryptedBytesStream);
      assertThat(decryptedAddress).isEqualTo(senderAddress);
    }
  }

  @Test
  public void sameSharesForSameDeterministicInput() {
    int input = 13;
    long seed = 12345;

    CompactBitArray secretVarData1 = secretVarDataFromInt(input);
    CompactBitArray secretVarData2 = secretVarDataFromInt(input);

    assertThat(new BinarySecretShares(secretVarData1, new Random(seed)).secretShares)
        .usingRecursiveComparison()
        .isEqualTo(new BinarySecretShares(secretVarData2, new Random(seed)).secretShares);
  }

  @Test
  public void differentSharesWhenUsingNonDeterministicRng() {
    int input = 13;

    CompactBitArray secretVarData1 = secretVarDataFromInt(input);
    CompactBitArray secretVarData2 = secretVarDataFromInt(input);

    BinarySecretShares nonDeterministicShares1 = new BinarySecretShares(secretVarData1);
    BinarySecretShares nonDeterministicShares2 = new BinarySecretShares(secretVarData2);

    assertThat(nonDeterministicShares1)
        .usingRecursiveComparison()
        .isNotEqualTo(nonDeterministicShares2);
  }

  static CompactBitArray secretVarDataFromInt(int secret) {
    return BitOutput.serializeBits(s -> s.writeSignedInt(secret, 32));
  }

  private BinaryShare shareFromBytes(byte[] toConvert) {
    List<BinaryExtensionFieldElement> elements = new ArrayList<>();
    for (byte b : toConvert) {
      elements.add(BinaryExtensionFieldElement.create(b));
    }
    return new BinaryShare(elements);
  }

  /**
   * Creates an AES decryption cipher from a shared key. The shared key is generated from an
   * ephemeral (temporary) public key, and the private key of the receiver of the encrypted message.
   *
   * @param sharedKey the shared key to generate the cipher from
   * @return the AES cipher
   */
  private static Cipher createDecryptionCipher(Hash sharedKey) {
    return ExceptionConverter.call(
        () -> {
          Cipher aes = Cipher.getInstance("AES/CBC/PKCS5Padding");
          IvParameterSpec ivParameterSpec =
              new IvParameterSpec(Arrays.copyOfRange(sharedKey.getBytes(), 0, 16));
          SecretKeySpec secretKeySpec =
              new SecretKeySpec(Arrays.copyOfRange(sharedKey.getBytes(), 16, 32), "AES");
          aes.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
          return aes;
        },
        "Unable to initialize AES");
  }
}
