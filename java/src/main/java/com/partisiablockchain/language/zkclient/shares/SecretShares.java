package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.CompactBitArray;

/**
 * The secret shares in a secret sharing scheme, used for inputs in ZK computations to keep the
 * underlying secret values hidden during and after the ZK computation.
 */
public interface SecretShares {

  /**
   * Secret share the given data.
   *
   * @param variableData the data to secret share.
   */
  void secretShare(CompactBitArray variableData);

  /**
   * Reconstruct the underlying secret data from the secret shares.
   *
   * @return the reconstructed secret data.
   */
  CompactBitArray reconstructSecret();
}
