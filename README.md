# ZK-client

Enables sending ZK inputs and retrieving shares from nodes.

Polyglot repository that contains [java](java/README.md) and [typescript](ts/README.md) libraries.