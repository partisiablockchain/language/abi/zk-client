/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Client, CryptoUtils, RealZkClient } from "../../main";
import { BitInput, BitOutput, CompactBitArray } from "@secata-public/bitmanipulation-ts";
import {
  BlockchainTransactionClient,
  SenderAuthenticationKeyPair,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { SignatureProviderKeyPair } from "../../main";

/** Address of the testnet node to request shares from. */
const TESTNET_URL = "https://node1.testnet.partisiablockchain.com";

/**
 * Private key of the account interacting with the blockchain. This account should have enough gas
 * on the testnet.
 */
const SENDER_PRIVATE_KEY = "bb";

const ZK_CONTRACT = "03f311860787e0413f0ffe785af52157922edce54c";

/** Identifier of the secret input action in the zk contract. */
const SECRET_INPUT_INVOCATION = 0x40;

/** The secret input value. */
const SECRET_INPUT_VALUE = 1343;

/**
 * ID of the variable to reconstruct. This variable must be owned by SENDER_PRIVATE_KEY for the
 * manual test to work.
 */
const VARIABLE_ID = 28;

/**
 * Starts a blockchain client for the testnet then reconstructs a secret variable in a contract
 * using default values. Should be run manually.
 */
async function main() {
  // Starts a blockchain client for the testnet
  const client = new Client(TESTNET_URL);

  const zkContract = await client.getContractState(ZK_CONTRACT);
  if (zkContract === undefined) {
    throw new Error("Couldn't get contract data");
  }
  const realZkClient: RealZkClient = RealZkClient.create(ZK_CONTRACT, client);

  const owner = CryptoUtils.privateKeyToKeypair(SENDER_PRIVATE_KEY);

  // Send secret variable on chain

  const serializedInput = BitOutput.serializeBits((out) =>
    out.writeSignedNumber(SECRET_INPUT_VALUE, 32)
  );

  const additionalRpc = Buffer.from([SECRET_INPUT_INVOCATION]);
  const sender = CryptoUtils.privateKeyToAccountAddress(SENDER_PRIVATE_KEY);

  const onChainInputTransaction = await realZkClient.buildOnChainInputTransaction(
    sender,
    serializedInput,
    additionalRpc
  );

  const transactionSender = BlockchainTransactionClient.create(
    TESTNET_URL,
    new SenderAuthenticationKeyPair(CryptoUtils.privateKeyToKeypair(SENDER_PRIVATE_KEY))
  );

  const sentTransaction = await transactionSender.signAndSend(onChainInputTransaction, 100000);

  const txIdentifier = sentTransaction.transactionPointer.identifier;

  // eslint-disable-next-line no-console
  console.log("Sent input in transaction: " + txIdentifier);

  const reconstructedSecret: CompactBitArray = await realZkClient.fetchSecretVariable(
    new SignatureProviderKeyPair(owner),
    VARIABLE_ID
  );

  const reconstructedInt = new BitInput(reconstructedSecret.data).readSignedNumber(32);
  // eslint-disable-next-line no-console
  console.log(reconstructedInt);
}

main();
