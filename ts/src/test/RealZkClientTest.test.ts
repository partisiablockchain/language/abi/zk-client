/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import fetchMock from "jest-fetch-mock";
import { BitOutput } from "@secata-public/bitmanipulation-ts";
import { CryptoUtils, RealZkClient } from "../main";
import { Client } from "../main";
// eslint-disable-next-line import/no-internal-modules
import * as fetchSecretFromNodes from "./json-request-response/fetchSecretFromNodes.json";

/** Address of the testnet node to request shares from. */
const TESTNET_URL = "https://node1.testnet.partisiablockchain.com";

/** Private keys of the accounts interacting with the blockchain. */
const OWNER_PRIVATE_KEY = "6d123bc5ed7632934c9155b8726de858a20e85cbd578e12f2b863d852c04343a";

/** Address of the zk-contract. */
const ZK_CONTRACT = "03dea8cf48d387618bdcd7033885ef24b11f4a933b";

/** Identifier of the secret input action in the zk contract. */
const SECRET_INPUT_INVOCATION = 0x40;

/** The secret input value. */
const SECRET_INPUT_VALUE = 10000;

describe("real zk client", () => {
  /** Build a transaction twice, should produce different transactions because of the underlying
   * secret sharing is different each time. */
  test("create real zk client", async () => {
    const serializedInput = BitOutput.serializeBits((out) =>
      out.writeSignedNumber(SECRET_INPUT_VALUE, 32)
    );
    const additionalRpc = Buffer.from([SECRET_INPUT_INVOCATION]);

    const client = new Client(TESTNET_URL);

    fetchMock.mockResponseOnce(
      JSON.stringify({
        serializedContract: fetchSecretFromNodes["0"].response.prettyEntity.serializedContract,
      })
    );
    const realZkClient = await RealZkClient.create(ZK_CONTRACT, client);

    const sender = CryptoUtils.privateKeyToAccountAddress(OWNER_PRIVATE_KEY);

    const onChainInputTransaction = await realZkClient.buildOnChainInputTransaction(
      sender,
      serializedInput,
      additionalRpc
    );

    const anotherOnChainInputTransaction = await realZkClient.buildOnChainInputTransaction(
      sender,
      serializedInput,
      additionalRpc
    );

    expect(onChainInputTransaction.rpc).not.toEqual(anotherOnChainInputTransaction.rpc);
  });

  test("fail to get zk nodes", async () => {
    const client = new Client(TESTNET_URL);
    const zkClient = RealZkClient.create(ZK_CONTRACT, client);
    await expect(zkClient.getZkContractNodes()).rejects.toThrowError("Couldn't get contract data");
  });
});
