package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElementFactory;
import com.partisiablockchain.zk.real.protocol.field.Lagrange;
import com.partisiablockchain.zk.real.protocol.field.Polynomial;
import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import com.secata.stream.SafeDataInputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Binary data which has been broken into secret shares. BinarySecretShares are distributed among ZK
 * nodes when inputting a secret variable, and received from ZK nodes when reconstructing a secret
 * variable.
 */
public final class BinarySecretShares implements SecretShares {

  private final Random rng;
  static final List<BinaryExtensionFieldElement> ALPHAS =
      new BinaryExtensionFieldElementFactory().alphas();
  List<BinaryShare> secretShares;

  /**
   * Constructs secret shares from a list of shares. The list contains a share for each
   * sending/receiving party of the secret data.
   *
   * @param secretShares the list of shares
   * @param rng the randomness to use for sharing.
   */
  BinarySecretShares(List<BinaryShare> secretShares, Random rng) {
    this.secretShares = secretShares;
    this.rng = rng;
  }

  BinarySecretShares(CompactBitArray variableData) {
    this(variableData, new SecureRandom());
  }

  /**
   * Create binary secret shares from the given bits, using the randomness provided to create the
   * polynomial.
   *
   * @param variableData the secret variable data to secret share.
   * @param rng the randomness to create the polynomial from.
   */
  public BinarySecretShares(CompactBitArray variableData, Random rng) {
    this(rng);
    this.secretShare(variableData);
  }

  private BinarySecretShares(Random rng) {
    this.rng = rng;
  }

  /**
   * Constructs secret shares from a list of shares. The list contains a share for each
   * sending/receiving party of the secret data.
   *
   * @param rawShares the shares of the secret variable.
   * @return a new binary secret sharing.
   */
  public static BinarySecretShares read(List<byte[]> rawShares) {
    return new BinarySecretShares(
        rawShares.stream()
            .map(SafeDataInputStream::createFromBytes)
            .map(BinaryShare::read)
            .toList(),
        null);
  }

  @Override
  public void secretShare(CompactBitArray variableData) {

    List<List<BinaryExtensionFieldElement>> sharedElements = new ArrayList<>(ALPHAS.size());
    for (int i = 0; i < ALPHAS.size(); i++) {
      sharedElements.add(i, new ArrayList<>());
    }

    for (int i = 0; i < variableData.size(); i++) {
      BinaryExtensionFieldElement secret =
          isBitSet(variableData.data(), i)
              ? BinaryExtensionFieldElement.ONE
              : BinaryExtensionFieldElement.ZERO;

      Polynomial<BinaryExtensionFieldElement> polynomial = generatePolynomial(secret);
      for (int j = 0; j < ALPHAS.size(); j++) {
        BinaryExtensionFieldElement share = polynomial.evaluate(ALPHAS.get(j));
        sharedElements.get(j).add(share);
      }
    }

    this.secretShares = sharedElements.stream().map(BinaryShare::new).toList();
  }

  /**
   * Reconstruct the secret variable data from these BinarySecretShares. First the shares of each
   * bit of the secret variable data is grouped. Then, a polynomial is interpolated from the shares
   * of each bit. The constant term of this polynomial is the value of the secret bit. Lastly, the
   * secret bits are collected in a byte array to form the secret variable data.
   *
   * @return the reconstructed binary secret variable data
   */
  @Override
  public CompactBitArray reconstructSecret() {
    return BitOutput.serializeBits(this::reconstructSecretBits);
  }

  /**
   * Generates a random polynomial of degree 1:
   *
   * <p><i>f(x)= secret + random*x</i>
   *
   * <p>such that f(0) match the provided secret.
   *
   * @param secret the secret to be embedded in the constant term
   * @return a random polynomial generated with the secret and a random number as coefficients.
   */
  private Polynomial<BinaryExtensionFieldElement> generatePolynomial(
      BinaryExtensionFieldElement secret) {
    byte[] randomByte = new byte[1];
    rng.nextBytes(randomByte);
    final BinaryExtensionFieldElement random = BinaryExtensionFieldElement.create(randomByte[0]);
    final List<BinaryExtensionFieldElement> coefficients = List.of(secret, random);
    return new Polynomial<>(coefficients, BinaryExtensionFieldElement.ZERO);
  }

  /**
   * Encrypts the secret shares to prepare them for sending on-chain. Each share is encrypted using
   * a shared key. The shared key is created from an ephemeral (temporary) private key and the
   * public key of the receiving engine. This allows only the receiving engine to recover the value
   * of the share. The private key is ephemeral such that two identical shares will be encrypted
   * differently in order to prevent replay attacks.
   *
   * @param ephemeralKey The temporary key to encrypt the shares with
   * @param sender the address of sender of the secret input
   * @param engineKeys the keys of the engines that are to receive the encrypted shares
   * @return the encrypted shares
   */
  public EncryptedBinaryShares encrypt(
      KeyPair ephemeralKey, BlockchainAddress sender, List<BlockchainPublicKey> engineKeys) {
    if (secretShares.size() != engineKeys.size()) {
      throw new RuntimeException(
          "Number of engines (%d) did not match number of shares (%d)"
              .formatted(engineKeys.size(), secretShares.size()));
    }

    final ArrayList<EncryptedBinaryShares.Share> shares = new ArrayList<>();
    for (int i = 0; i < engineKeys.size(); i++) {
      BinaryShare share = secretShares.get(i);
      BlockchainPublicKey engineKey = engineKeys.get(i);
      final EncryptedBinaryShares.Share encryptedShare =
          share.encrypt(ephemeralKey, sender, engineKey);
      shares.add(encryptedShare);
    }
    return new EncryptedBinaryShares(shares);
  }

  /**
   * Applies blinding to the shares to prepare them for sending off-chain. If blinding were not
   * applied, secret shares could easily be guessed for small input sizes, since hashes of the
   * shares are put directly on the blockchain.
   *
   * @return the shares concatenated with blindings
   */
  public BlindedBinaryShares applyBlinding() {
    Blindings blindings = Blindings.generate(noOfShares(), rng);
    final ArrayList<BlindedBinaryShares.Share> blindedShares = new ArrayList<>();
    for (int i = 0; i < blindings.getBlindings().size(); i++) {
      final BinaryShare share = secretShares.get(i);
      final Blindings.Blinding blinding = blindings.getBlindings().get(i);
      final BlindedBinaryShares.Share blindedShare = share.serializeWithBlinding(blinding);
      blindedShares.add(blindedShare);
    }

    return new BlindedBinaryShares(blindedShares);
  }

  /**
   * Returns the amount of shares.
   *
   * @return the size of the share list
   */
  public int noOfShares() {
    return secretShares.size();
  }

  /**
   * Get the bitlength of the secret sharing.
   *
   * @return the numver of bits secret shared.
   */
  public int getShareBitLength() {
    return secretShares.get(0).bitElements.size();
  }

  /**
   * Reconstruct each bit and write them to the bit output.
   *
   * @param bitOutput bit output stream to write to
   */
  private void reconstructSecretBits(BitOutput bitOutput) {
    for (int i = 0; i < getShareBitLength(); i++) {
      bitOutput.writeBoolean(reconstructSecretBit(i));
    }
  }

  /**
   * Reconstructs one bit of the secret variable data which these binary secret shares constitute. A
   * polynomial is interpolated from the shares of the bit, and the constant term of this polynomial
   * is returned as the reconstructed bit.
   *
   * @param i index of the bit to reconstruct
   * @return the reconstructed bit
   */
  private boolean reconstructSecretBit(int i) {
    List<BinaryExtensionFieldElement> sharesOfBit =
        secretShares.stream().map(share -> share.bitElements.get(i)).collect(Collectors.toList());
    Polynomial<BinaryExtensionFieldElement> polynomial = interpolatePolynomial(sharesOfBit);
    return !polynomial.getConstantTerm().isZero();
  }

  /**
   * Interpolates a polynomial from a list of shares using lagrange interpolation.
   *
   * @param shares the shares which acts as y-coordinates of the points on the polynomial
   * @return the interpolated polynomial
   */
  private static Polynomial<BinaryExtensionFieldElement> interpolatePolynomial(
      List<BinaryExtensionFieldElement> shares) {
    return Lagrange.interpolate(
        ALPHAS, shares, 1, BinaryExtensionFieldElement.ZERO, BinaryExtensionFieldElement.ONE);
  }

  private static boolean isBitSet(byte[] data, int index) {
    final int byteIndex = index / 8;
    final byte byteValue = data[byteIndex];
    final int bitIndex = index % 8;
    final int bitValue = byteValue & (1 << bitIndex);
    return bitValue != 0;
  }
}
