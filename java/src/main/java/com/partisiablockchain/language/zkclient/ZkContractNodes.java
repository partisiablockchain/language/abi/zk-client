package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.zkclient.shares.BinarySecretShares;
import com.partisiablockchain.language.zkclient.shares.BlindedBinaryShares;
import java.util.List;

/** Zk nodes allocated to a specific Zk contract. Used to send and fetch shares from the nodes. */
public interface ZkContractNodes {

  /**
   * Fetch a secret variable from the Zk nodes associated to a specific contract. The secret must be
   * owned by the account signing the request.
   *
   * @param senderAuthentication the authentication used to prove ownership of variable.
   * @param variableId the id of the variable to fetch.
   * @return the binary secret shares for the given variable.
   */
  BinarySecretShares fetchSharesFromNodes(
      SenderAuthentication senderAuthentication, int variableId);

  /**
   * Sends blinded secret shares to the ZK nodes over https.
   *
   * @param transactionHash the verification transaction the nodes can verify the share they receive
   *     with.
   * @param sender the sender of the off-chain input
   * @param zkContractAddress the address the input is inputted to.
   * @param blindedShares the blinded shares to send to the nodes.
   */
  void sendSharesToNodes(
      Hash transactionHash,
      BlockchainAddress sender,
      BlockchainAddress zkContractAddress,
      BlindedBinaryShares blindedShares);

  /**
   * Get the public keys of the nodes.
   *
   * @return the list of public keys.
   */
  List<BlockchainPublicKey> getPublicKeysOfNodes();

  /**
   * Get the rest inteface urls of the nodes.
   *
   * @return a list of rest interface urls of the nodes.
   */
  List<String> getRestInterfacesOfNodes();

  /**
   * Create signatures for fetching secret shares from each node. The zk contract nodes use these
   * signatures to verify that a fetch request of a secret share is valid.
   *
   * <p>These signatures can be created by signing the hash created by {@link
   * ZkContractNode#hashRequest}.
   *
   * @param owner the authentication for the owner of the variable.
   * @param variableId the id of the variable to fetch.
   * @return a list of created signatures.
   */
  List<Signature> createFetchSignatures(SenderAuthentication owner, int variableId);
}
