/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { CryptoUtils, SignatureProviderKeyPair } from "../../main";

test("create signature provider with key pair", async () => {
  const keyPair = CryptoUtils.privateKeyToKeypair("01");
  const signatureProvider = new SignatureProviderKeyPair(keyPair);

  const keyPair2 = CryptoUtils.privateKeyToKeypair("02");
  const signatureProvider2 = new SignatureProviderKeyPair(keyPair2);

  const messageToSign = Buffer.from("test");

  const signatureBuffer = await signatureProvider.sign(messageToSign);
  const signatureBuffer2 = await signatureProvider2.sign(messageToSign);

  expect(signatureBuffer).not.toBeNull();
  expect(signatureBuffer2).not.toBeNull();
  expect(signatureBuffer).not.toEqual(signatureBuffer2);
});
