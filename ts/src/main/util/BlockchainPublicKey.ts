/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** A public key on the PBC blockchain. */
export class BlockchainPublicKey {
  private readonly val: Buffer;
  static readonly PUBLIC_KEY_LENGTH = 33;
  private constructor(val: Buffer) {
    this.val = val;
  }

  /**
   * Returns the blockchain public key encoded as a hex string.
   *
   * @return the hex string.
   */
  public asString(): string {
    return this.val.toString("hex");
  }

  /**
   * Returns the blockchain public key as bytes.
   *
   * @return the bytes.
   */
  public asBuffer(): Buffer {
    return Buffer.from(this.val);
  }

  /**
   * Creates a public key from a hex string.
   *
   * @param val the encoded public key.
   * @return the blockchain public key.
   */
  public static fromString(val: string): BlockchainPublicKey {
    const buffer = Buffer.from(val, "hex");
    this.validateLength(buffer);
    return new BlockchainPublicKey(buffer);
  }

  /**
   * Creates a public key from bytes.
   *
   * @param val the encoded public key.
   * @return the blockchain public key.
   */
  public static fromBuffer(val: Buffer): BlockchainPublicKey {
    const buffer = Buffer.from(val);
    this.validateLength(buffer);
    return new BlockchainPublicKey(val);
  }

  private static validateLength(val: Buffer) {
    if (val.length !== BlockchainPublicKey.PUBLIC_KEY_LENGTH) {
      throw new Error(
        `BlockchainPublicKey expects exactly ${BlockchainPublicKey.PUBLIC_KEY_LENGTH} bytes, but found ${val.length}`
      );
    }
  }
}
