package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

final class RealZkClientTest {

  private static final String TESTNET = "https://node1.testnet.partisiablockchain.com";
  private static final BlockchainAddress ZK_CONTRACT =
      BlockchainAddress.fromString("03ba8037ee2ffc597924ba42c7982b9bd2229a33a1");
  private static final SenderAuthenticationKeyPair OWNER_KEY =
      SenderAuthenticationKeyPair.fromString(
          "6d123bc5ed7632934c9155b8726de858a20e85cbd578e12f2b863d852c04343a");

  private static RealZkClient zkClient;

  @BeforeAll
  static void createClients() {
    BlockchainClient blockchainClient = BlockchainClient.create(TESTNET, 3);
    Client client =
        ClientBuilder.newBuilder()
            .connectTimeout(30L, TimeUnit.SECONDS)
            .readTimeout(30L, TimeUnit.SECONDS)
            .build();

    WebClient webClient = new JerseyWebClient(client);

    zkClient = RealZkClient.create(ZK_CONTRACT, webClient, blockchainClient);
  }

  /**
   * Build a transaction twice, should produce different transactions because of the underlying
   * secret sharing is different each time.
   */
  @Test
  void identicalInputsProduceDifferentTransactions() {

    Transaction firstTransaction =
        zkClient.buildOnChainInputTransaction(
            OWNER_KEY.getAddress(), createSecretIntInput(10000), averageSalaryPublicRpc());

    Transaction secondTransaction =
        zkClient.buildOnChainInputTransaction(
            OWNER_KEY.getAddress(), createSecretIntInput(10000), averageSalaryPublicRpc());

    Assertions.assertThat(firstTransaction).isNotEqualTo(secondTransaction);
  }

  /**
   * Zk Client returns all the rest interface urls for all the mpc nodes associated with the
   * contract.
   */
  @Test
  void canGetRestInterfacesOfNodes() {
    List<String> restInterfaces = zkClient.getNodeRestInterfaces();

    Assertions.assertThat(restInterfaces.size()).isEqualTo(4);
    Assertions.assertThat(restInterfaces).doesNotHaveDuplicates();
    for (String restInterface : restInterfaces) {
      Assertions.assertThat(restInterface).contains("testnet.partisiablockchain.com");
    }
  }

  /**
   * RealZkClient returns a signature, required to request a secret shared variable, from each mpc
   * nodes associated with the contract.
   */
  @Test
  void canCreateVariableRequestSignaturesFromKey() {
    List<Signature> signatures = zkClient.getRequestSignatures(OWNER_KEY, 0);

    Assertions.assertThat(signatures.size()).isEqualTo(4);
    Assertions.assertThat(signatures).doesNotHaveDuplicates();
    for (Signature signature : signatures) {
      Assertions.assertThat(signature).isNotNull();
    }
  }

  private byte[] averageSalaryPublicRpc() {
    return new byte[] {0x40};
  }

  CompactBitArray createSecretIntInput(int secret) {
    return BitOutput.serializeBits(output -> output.writeSignedInt(secret, 32));
  }
}
