package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataOutputStream;
import java.util.List;

/**
 * Encrypted secret shares of binary data. The secret shares have been encrypted for each of the
 * parties (engines) that will receive them. The order of the encrypted shares match the order of
 * the engines.
 */
public final class EncryptedBinaryShares {
  private final List<Share> shares;

  /**
   * Constructs encrypted shares from a list of shares. The list contains an encrypted share for
   * each receiving party of the secret input.
   *
   * @param shares the encrypted shares
   */
  EncryptedBinaryShares(List<Share> shares) {
    this.shares = shares;
  }

  /**
   * Returns the encrypted shares.
   *
   * @return the shares
   */
  public List<Share> getShares() {
    return shares;
  }

  /**
   * An encrypted secret share. Secret shares are disjoint parts of a secret input.
   *
   * @param encrypted the serialized share
   */
  @SuppressWarnings("ArrayRecordComponent")
  record Share(byte[] encrypted) {}

  /**
   * Serialize the encrypted shares by concatenating their byte representation.
   *
   * @return the concatenated shares
   */
  public byte[] serialize() {
    final byte[] concatenatedEncryptedShares =
        SafeDataOutputStream.serialize(
            stream -> {
              for (Share encryptedShare : shares) {
                stream.write(encryptedShare.encrypted());
              }
            });
    return concatenatedEncryptedShares;
  }
}
