/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Client } from "../../main";
import fetchMock from "jest-fetch-mock";

let client: Client;

describe("Client tests", () => {
  beforeEach(async () => {
    client = new Client("some_host");
  });

  test("0 shards", () => {
    fetchMock.resetMocks();
    fetchMock.mockOnce("test-response");
    client = new Client("some_host", 0);
    client.getContractState("030e21bc677365ffb6f786436ca0771581b1b4a119");
    const url = fetchMock.mock.calls[0]["0"];
    expect(url).toEqual(
      "some_host/blockchain/contracts/030e21bc677365ffb6f786436ca0771581b1b4a119?requireContractState=true"
    );
  });
});
