package com.partisiablockchain.language.zkclient.node;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.language.zkclient.ZkContractNode;
import com.partisiablockchain.language.zkclient.ZkContractNodes;
import com.partisiablockchain.language.zkclient.ZkNodes;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.ArrayList;
import java.util.List;

/** Get node information about the REAL Nodes associated with a contract. */
public final class RealZkNodes implements ZkNodes {

  private final WebClient webClient;
  private final BlockchainClient blockchainClient;

  RealZkNodes(WebClient webClient, BlockchainClient blockchainClient) {
    this.webClient = webClient;
    this.blockchainClient = blockchainClient;
  }

  /**
   * Create the RealZkNodes used for communicating with the nodes and chain when sending inputs and
   * getting secrets from the nodes.
   *
   * @param webClient the client used for direct communication between a user and the nodes.
   * @param blockchainClient the client used for sending the blockchain transactions.
   * @return the nodes to send interactions with.
   */
  public static RealZkNodes create(WebClient webClient, BlockchainClient blockchainClient) {
    return new RealZkNodes(webClient, blockchainClient);
  }

  @Override
  public ZkContractNodes getNodes(BlockchainAddress contractAddress) {

    ContractState zkContract = blockchainClient.getContractState(contractAddress);
    JsonNode nodeList = zkContract.serializedContract().get("engines").get("engines");

    List<ZkContractNode> nodes = new ArrayList<>();
    for (int i = 0; i < nodeList.size(); i++) {
      JsonNode engineInformation = nodeList.get(i);

      BlockchainAddress identity =
          ExceptionConverter.call(
              () -> BlockchainAddress.fromString(engineInformation.get("identity").textValue()),
              "Unable to read identity for engine");

      BlockchainPublicKey publicKey =
          ExceptionConverter.call(
              () ->
                  BlockchainPublicKey.fromEncodedEcPoint(
                      engineInformation.get("publicKey").binaryValue()),
              "Unable to read public key for engine");

      String restInterface =
          ExceptionConverter.call(
              () -> engineInformation.get("restInterface").textValue(),
              "Unable to read rest interface for engine");

      nodes.add(
          new RealContractNode(webClient, contractAddress, identity, publicKey, restInterface));
    }

    return new RealContractNodes(nodes);
  }
}
