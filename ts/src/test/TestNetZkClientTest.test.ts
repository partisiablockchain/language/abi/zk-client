/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import fetchMock from "jest-fetch-mock";
import {
  BigEndianByteOutput,
  BitInput,
  BitOutput,
  CompactBitArray,
} from "@secata-public/bitmanipulation-ts";
import { CryptoUtils, RealZkClient } from "../main";
import { Client } from "../main";
import { OffChainInput } from "../main";
// eslint-disable-next-line import/no-internal-modules
import * as fetchSecretFromNodes from "./json-request-response/fetchSecretFromNodes.json";
// eslint-disable-next-line import/no-internal-modules
import * as secretFetchedByAnotherAccountThanTheOwner from "./json-request-response/secretFetchedByAnotherAccountThanTheOwner.json"; // eslint-disable-next-line import/no-internal-modules
// eslint-disable-next-line import/no-internal-modules
import * as sendSecretInputOffChain from "./json-request-response/sendSecretInputOffChain.json";
// eslint-disable-next-line import/no-internal-modules
import * as sendSecretInputOnChain from "./json-request-response/sendSecretInputOnChain.json";
import { ec } from "elliptic";
import KeyPair = ec.KeyPair;
import {
  BlockchainTransactionClient,
  Chain,
  SenderAuthenticationKeyPair,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { SignatureProviderKeyPair } from "../main/util/SignatureProviderKeyPair";

/** Address of the testnet node to request shares from. */
const TESTNET_URL = "https://node1.testnet.partisiablockchain.com";

/** Header used for creating messages. */
const VARIABLE_HEADER = ["s", "h", "a", "m", "i", "r", "v", "a", "r", "i", "a", "b", "l", "e"].map(
  (s) => s.charCodeAt(0)
);

/** Private keys of the accounts interacting with the blockchain. */
const OWNER_PRIVATE_KEY = "6d123bc5ed7632934c9155b8726de858a20e85cbd578e12f2b863d852c04343a";

const USER1_PRIVATE_KEY = "1bfa9425af347e9b2592c3fb608d51a761e44778e9dae276c04a020f1acd704c";

/** Address of the zk-contract. */
const ZK_CONTRACT = "032a9cfb81515fc8ee23a46d722e170d0c927972de";

/** Identifier of the secret input action in the zk contract. */
const SECRET_INPUT_INVOCATION = 0x40;

/** The secret input value. */
const SECRET_INPUT_VALUE = 10000;

/** An account. */
const OWNER = CryptoUtils.privateKeyToKeypair(OWNER_PRIVATE_KEY);

/** A different account. */
const USER1 = CryptoUtils.privateKeyToKeypair(USER1_PRIVATE_KEY);

describe("zk client", () => {
  let realZkClient: RealZkClient;
  let transactionSender: BlockchainTransactionClient;

  const serializedInput = BitOutput.serializeBits((out) =>
    out.writeSignedNumber(SECRET_INPUT_VALUE, 32)
  );
  const additionalRpc = Buffer.from([SECRET_INPUT_INVOCATION]);

  /** Create a RealZkClient by mocking the serialized contract. */
  beforeEach(async () => {
    fetchMock.resetMocks();

    transactionSender = BlockchainTransactionClient.create(
      TESTNET_URL,
      new SenderAuthenticationKeyPair(CryptoUtils.privateKeyToKeypair(OWNER_PRIVATE_KEY))
    );

    const client = new Client(TESTNET_URL);

    realZkClient = await RealZkClient.createForTest(ZK_CONTRACT, client, () =>
      Buffer.from("veryrandom")
    );

    fetchMock.resetMocks();
  });

  /** Nodes in the RealZkClient should be the same as the nodes in the mocked serialized contract. */
  test("create real zk client", async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify({
        serializedContract: fetchSecretFromNodes["0"].response.prettyEntity.serializedContract,
      })
    );
    const expectedKeys = fetchSecretFromNodes[
      "0"
    ].response.prettyEntity.serializedContract.engines.engines.map((node) => node.publicKey);
    expect(
      (await realZkClient.getZkContractNodes())
        .getPublicKeysOfNodes()
        .map((key) => key.asBuffer().toString("base64"))
    ).toEqual(expectedKeys);

    const calls = fetchMock.mock.calls;

    expect(calls[0]["1"]?.method).toEqual(fetchSecretFromNodes["0"].request.httpMethod.toString());
    expect(calls[0]["0"]).toContain(fetchSecretFromNodes["0"].request.url.slice(0, 123).toString());
  });

  /** A user can send secret input on chain, i.e. the call methods and URLs match the recorded calls except for randomness. */
  test("send secret input on chain", async () => {
    const sender = CryptoUtils.privateKeyToAccountAddress(OWNER_PRIVATE_KEY);
    fetchMock.mockResponseOnce(
      JSON.stringify({
        serializedContract: sendSecretInputOnChain["0"].response.prettyEntity.serializedContract,
      })
    );
    const onChainInputTransaction = await realZkClient.buildOnChainInputTransaction(
      sender,
      serializedInput,
      additionalRpc
    );

    fetchMock.resetMocks();
    fetchMock.mockResponseOnce(JSON.stringify(sendSecretInputOnChain["1"].response.prettyEntity)); // nonce

    const chainId = sendSecretInputOnChain["2"].response.prettyEntity.chainId;
    const chainResponse: Chain = {
      governanceVersion: 0,
      chainId,
      shards: [],
      plugins: {},
      features: [],
    };
    fetchMock.mockResponseOnce(JSON.stringify(chainResponse)); // chainId
    fetchMock.mockResponseOnce("{}"); // put transaction response

    await transactionSender.signAndSend(onChainInputTransaction, 100000);

    const calls = fetchMock.mock.calls;

    // On-chain

    // GET request
    expect(calls["0"]["1"]?.method).toEqual(sendSecretInputOnChain["1"].request.httpMethod);
    expect(calls["0"]["0"]).toEqual(
      `${TESTNET_URL}/chain/accounts/00683441681a1ab734ac9e68184607ea90bfc229af`
    );

    // GET request
    expect(calls["1"]["1"]?.method).toEqual(sendSecretInputOnChain["2"].request.httpMethod);
    expect(calls["1"]["0"]).toEqual(`${TESTNET_URL}/chain`);

    // PUT request
    expect(calls["2"]["1"]?.method).toEqual(sendSecretInputOnChain["3"].request.httpMethod);
    expect(calls["2"]["0"]).toEqual(`${TESTNET_URL}/chain/transactions`);
  });

  /** A user can send secret input off chain, i.e. the call methods and URLs match the recorded calls except for randomness. */
  test("send secret input off chain", async () => {
    const serializedInput = BitOutput.serializeBits((out) =>
      out.writeSignedNumber(SECRET_INPUT_VALUE, 32)
    );
    const additionalRpc = Buffer.from([SECRET_INPUT_INVOCATION]);

    const offChainInput: OffChainInput = realZkClient.buildOffChainInputTransaction(
      serializedInput,
      additionalRpc
    );

    fetchMock.resetMocks();
    fetchMock.mockResponseOnce(JSON.stringify(sendSecretInputOffChain["0"].response.prettyEntity)); // nonce

    const chainId = sendSecretInputOffChain["1"].response.prettyEntity.chainId;
    const chainResponse: Chain = {
      governanceVersion: 0,
      chainId,
      shards: [],
      plugins: {},
      features: [],
    };
    fetchMock.mockResponseOnce(JSON.stringify(chainResponse)); // chainId
    fetchMock.mockResponseOnce("{}"); // put transaction response

    const sentTransaction = await transactionSender.signAndSend(offChainInput.transaction, 100000);

    const txIdentifier = sentTransaction.transactionPointer.identifier;

    // Get nodes
    fetchMock.mockResponseOnce(
      JSON.stringify({
        serializedContract: sendSecretInputOffChain["3"].response.prettyEntity.serializedContract,
      })
    );
    await realZkClient.sendOffChainInputToNodes(
      ZK_CONTRACT,
      CryptoUtils.privateKeyToAccountAddress(USER1_PRIVATE_KEY),
      txIdentifier,
      offChainInput.blindedShares
    );

    const calls = fetchMock.mock.calls;

    // On-chain

    // GET request
    expect(calls["0"]["1"]?.method).toEqual(sendSecretInputOffChain["0"].request.httpMethod);
    expect(calls["0"]["0"]).toEqual(
      `${TESTNET_URL}/chain/accounts/00683441681a1ab734ac9e68184607ea90bfc229af`
    );

    // GET request
    expect(calls["1"]["1"]?.method).toEqual(sendSecretInputOffChain["1"].request.httpMethod);
    expect(calls["1"]["0"]).toEqual(`${TESTNET_URL}/chain`);

    // PUT request
    expect(calls["2"]["1"]?.method).toEqual(sendSecretInputOffChain["2"].request.httpMethod);
    expect(calls["2"]["0"]).toEqual(`${TESTNET_URL}/chain/transactions`);

    // Send secret off-chain to all 4 nodes
    // All URL are expected to be of the following form for X = 1,2,3,4 and with some random bytes at the end:
    // `https://nodeX.testnet.partisiablockchain.com/real/binary/input/${ZK_CONTRACT.asString()}/${USER1_ADDRESS.asString()}/RANDOMNESS`

    // GET request
    expect(calls["3"]["1"]?.method).toEqual(
      sendSecretInputOffChain["3"].request.httpMethod.toString()
    );
    expect(calls["3"]["0"]).toContain(
      sendSecretInputOffChain["3"].request.url.slice(0, 123).toString()
    );

    // PUT request
    expect(calls["4"]["1"]?.method).toEqual(sendSecretInputOffChain["4"].request.httpMethod);
    expect(calls["4"]["0"]).toContain(sendSecretInputOffChain["4"].request.url.slice(0, 149));

    // PUT request
    expect(calls["5"]["1"]?.method).toEqual(sendSecretInputOffChain["5"].request.httpMethod);
    expect(calls["5"]["0"]).toContain(sendSecretInputOffChain["5"].request.url.slice(0, 149));

    // PUT request
    expect(calls["6"]["1"]?.method).toEqual(sendSecretInputOffChain["6"].request.httpMethod);
    expect(calls["6"]["0"]).toContain(sendSecretInputOffChain["6"].request.url.slice(0, 149));

    // PUT request
    expect(calls["7"]["1"]?.method).toEqual(sendSecretInputOffChain["7"].request.httpMethod);
    expect(calls["7"]["0"]).toContain(sendSecretInputOffChain["7"].request.url.slice(0, 149));
  });

  /** The owner can fetch the secret variable from the nodes. */
  test("fetch secret from nodes", async () => {
    fetchMock.resetMocks();
    fetchMock.mockResponseOnce(
      JSON.stringify({
        serializedContract: fetchSecretFromNodes["0"].response.prettyEntity.serializedContract,
      })
    );
    fetchMock.mockResponseOnce(
      JSON.stringify({ data: "AAAAAQAAACAICAABDAMGDQgJCgUFCA8NBQ8ECAsEDg0ECwcLAwIADA==" })
    );
    fetchMock.mockResponseOnce(
      JSON.stringify({ data: "AAAAAQAAACADAwACCAYMCQACBAoKAA0JCg0IAwUIDwkIBQ4FBgQACw==" })
    );
    fetchMock.mockResponseOnce(
      JSON.stringify({ data: "AAAAAQAAACALCwADBQUKBAkKDw8PCQIEDwIMCw4MAQQMDgkOBQYABw==" })
    );
    fetchMock.mockResponseOnce(
      JSON.stringify({ data: "AAAAAQAAACAGBgAEAAwLAQMHCwcHAwkBBwkDBgoDDQEDCg8KDAgABQ==" })
    );

    const reconstructedSecret: CompactBitArray = await realZkClient.fetchSecretVariable(
      new SignatureProviderKeyPair(OWNER),
      1
    );

    const calls = fetchMock.mock.calls;

    // All URLs are expected to be of the following form for X = 1,2,3,4, and with some random bytes at the end:
    // `https://nodeX.testnet.partisiablockchain.com/real/binary/output/variable/${ZK_CONTRACT.asString()}/1`

    // POST request
    expect(calls[1]["1"]?.method).toEqual(fetchSecretFromNodes["10"].request.httpMethod);
    expect(calls[1]["0"]).toContain(fetchSecretFromNodes["10"].request.url.slice(0, 116));

    // POST request
    expect(calls[2]["1"]?.method).toEqual(fetchSecretFromNodes["11"].request.httpMethod);
    expect(calls[2]["0"]).toContain(fetchSecretFromNodes["11"].request.url.slice(0, 116));

    // POST request
    expect(calls[3]["1"]?.method).toEqual(fetchSecretFromNodes["12"].request.httpMethod);
    expect(calls[3]["0"]).toContain(fetchSecretFromNodes["12"].request.url.slice(0, 116));

    // POST request
    expect(calls[4]["1"]?.method).toEqual(fetchSecretFromNodes["13"].request.httpMethod);
    expect(calls[4]["0"]).toContain(fetchSecretFromNodes["13"].request.url.slice(0, 116));

    const actualSignatures: string[] = calls
      .slice(1)
      .map((call) => JSON.parse(<string>call[1]?.body?.toString()).signature);
    const expectedSignatures = createAndSignMessages(1, OWNER);
    expect(actualSignatures).toEqual(expectedSignatures);

    const reconstructedInt = new BitInput(reconstructedSecret.data).readSignedNumber(32);
    expect(reconstructedInt).toEqual(SECRET_INPUT_VALUE);
  });

  /** Only the owner can fetch the secret variable from the nodes. */
  test("secret fetched by other account than the owner", async () => {
    fetchMock.resetMocks();
    fetchMock.mockResponseOnce(
      JSON.stringify({
        serializedContract:
          secretFetchedByAnotherAccountThanTheOwner["0"].response.prettyEntity.serializedContract,
      })
    );
    fetchMock.mockRejectOnce();
    fetchMock.mockRejectOnce();
    fetchMock.mockRejectOnce();
    fetchMock.mockRejectOnce();

    await expect(
      realZkClient.fetchSecretVariable(new SignatureProviderKeyPair(USER1), 1)
    ).rejects.toThrow("Result when fetching shares is undefined");

    const calls = fetchMock.mock.calls;

    // POST request
    expect(calls[1][1]?.method).toEqual(
      secretFetchedByAnotherAccountThanTheOwner["12"].request.httpMethod
    );

    const actualSignatures: string[] = calls
      .slice(1)
      .map((call) => JSON.parse(<string>call[1]?.body?.toString()).signature);

    // Recreate and resign the message to get the expected signatures
    const expectedSignatures = createAndSignMessages(1, USER1);

    // The request to fetch secret variables should be signed by USER1
    expect(actualSignatures).toEqual(expectedSignatures);
  });
});

function createAndSignMessages(variableId: number, owner: KeyPair): string[] {
  const sigs: string[] = [];
  for (const identity of secretFetchedByAnotherAccountThanTheOwner[
    "0"
  ].response.prettyEntity.serializedContract.engines.engines.map((node) => node.identity)) {
    const msg = BigEndianByteOutput.serialize((out) => {
      out.writeBytes(Buffer.from(VARIABLE_HEADER));
      out.writeBytes(Buffer.from(identity, "hex"));
      out.writeBytes(Buffer.from(ZK_CONTRACT, "hex"));
      out.writeI32(variableId);
    });
    const signature = CryptoUtils.signatureToBuffer(owner.sign(CryptoUtils.hashBuffer(msg)));
    sigs.push(signature.toString("base64"));
  }
  return sigs;
}
