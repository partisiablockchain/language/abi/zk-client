/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainAddress } from "@partisiablockchain/blockchain-api-transaction-client";
import { getRequest } from "./Api";

/** A client for interacting with contracts and accounts on a Partisia blockchain (PBC). */
export class Client {
  private readonly host: string;
  private readonly numOfShards: number;

  /**
   * Create a new client.
   *
   * @param host the url of the host.
   * @param numOfShards number of shards, default is 3.
   */
  constructor(host: string, numOfShards?: number) {
    this.host = host;
    this.numOfShards = numOfShards ?? 3;
  }

  private contractStateQueryUrl(address: BlockchainAddress): string {
    return `${this.host}${this.shardForAddress(
      address
    )}/blockchain/contracts/${address}?requireContractState=true`;
  }

  private shardForAddress(address: BlockchainAddress): string {
    if (this.numOfShards == 0) {
      return "";
    }
    const numOfShards = this.numOfShards;
    const buffer = Buffer.from(address, "hex");
    const shardIndex = Math.abs(buffer.readInt32BE(17)) % numOfShards;
    return "/shards/Shard" + shardIndex;
  }

  /**
   * Retrieve a contract state from the blockchain.
   *
   * @param address The address of the contract.
   * @return A promise containing the retrieved contract state.
   */
  public getContractState(address: BlockchainAddress): Promise<ZkContract | undefined> {
    const url = this.contractStateQueryUrl(address);
    return getRequest<ZkContract>(url);
  }
}

/** dto of a zk contract on the blockchain. */
export type ZkContract = {
  serializedContract: {
    attestations: [{ key: number; value: Attestation }];
    engines: { engines: Engine[] };
    variables: Variable[];
    openState: { openState: { data: string } };
  };
  abi: string;
};

export interface Attestation {
  data: { data: string };
  signatures: string[];
}

/** dto of an engine in the zk contract object. */
export interface Engine {
  /** Address of the engine. */
  identity: string;
  /** Public key of the engine encoded in base64. */
  publicKey: string;
  /** Rest interface of the engine. */
  restInterface: string;
}

export interface Variable {
  /** ID of variable. */
  key: string;
  /** Information about the secret variable. */
  value: {
    /** ID of variable. */
    id: number;
    /** The open state of this variable. */
    information: object;
    /** An offset pointing to the index of the first available input mask. */
    inputMaskOffSet: number;
    /** Serialized and masked user inputs. */
    maskedInputShare?: object;
    /** Blockchain address for the owner of the variable. */
    owner: string;
    /** Sealed variable is a variable that cannot be inspected - not even by the owner. */
    sealed: boolean;
    /** List describing the bit-lengths of the values that are stored in this secret variable. */
    shareBitLengths: number[];
    /** Hash identifying which transaction created this variable. */
    transaction?: string;
    /** Bytes for a variable that has been opened on the blockchain. */
    openValue?: object;
  };
}
