/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { RealContractNodes, timeout, TimeSleeper } from "../../main/node/RealContractNodes";
import { ZkContractNodeStub } from "./ZkContractNodeStub";
import { BlindedBinaryShares } from "../../main";

jest.setTimeout(20000);

describe("Retry tests", () => {
  const TRANSACTION_HASH = "testHash";
  const SENDER = "000000000000000000000000000000000000000000";
  const BLINDED_SHARES = new BlindedBinaryShares([Buffer.from("test")]);

  test("send shares with retries all success", async () => {
    const sleepCalled: number[] = [];

    const sleepFun: (n: number) => Promise<void> = (n: number) => {
      sleepCalled.push(n);
      return new Promise((resolve) => resolve());
    };

    const nodes = [
      new ZkContractNodeStub(0),
      new ZkContractNodeStub(0),
      new ZkContractNodeStub(0),
      new ZkContractNodeStub(0),
    ];

    const realContractNodes: RealContractNodes = new RealContractNodes(
      nodes,
      new TimeSleeper(sleepFun)
    );

    await realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, BLINDED_SHARES);

    expect(sleepCalled.length).toEqual(0);
    nodes.forEach((node) => expect(node.getSendCount()).toEqual(1));
  });

  test("send shares with retries all failure", async () => {
    const sleepCalled: number[] = [];

    const sleepFun: (n: number) => Promise<void> = (n: number) => {
      sleepCalled.push(n);
      return new Promise((resolve) => resolve());
    };

    const nodes = [
      new ZkContractNodeStub(10),
      new ZkContractNodeStub(10),
      new ZkContractNodeStub(10),
      new ZkContractNodeStub(10),
    ];

    const realContractNodes: RealContractNodes = new RealContractNodes(
      nodes,
      new TimeSleeper(sleepFun)
    );

    await expect(() =>
      realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, BLINDED_SHARES)
    ).rejects.toEqual(new Error("Unable to send shares to nodes"));

    expect(sleepCalled).toEqual([5000, 10000, 15000]);

    nodes.forEach((node) => expect(node.getSendCount()).toEqual(4));
  });

  test("send shares with retries", async () => {
    const sleepCalled: number[] = [];

    const sleepFun: (n: number) => Promise<void> = (n: number) => {
      sleepCalled.push(n);
      return new Promise((resolve) => resolve());
    };

    const nodes = [
      new ZkContractNodeStub(0),
      new ZkContractNodeStub(2),
      new ZkContractNodeStub(3),
      new ZkContractNodeStub(1),
    ];

    const realContractNodes: RealContractNodes = new RealContractNodes(
      nodes,
      new TimeSleeper(sleepFun)
    );

    await realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, BLINDED_SHARES);
    expect(sleepCalled).toEqual([5000, 10000, 15000]);

    expect(nodes[0].getSendCount()).toEqual(1);
    expect(nodes[1].getSendCount()).toEqual(3);
    expect(nodes[2].getSendCount()).toEqual(4);
    expect(nodes[3].getSendCount()).toEqual(2);
  });

  test("send shares with retries and less retries", async () => {
    const sleepCalled: number[] = [];

    const sleepFun: (n: number) => Promise<void> = (n: number) => {
      sleepCalled.push(n);
      return new Promise((resolve) => resolve());
    };

    const nodes = [
      new ZkContractNodeStub(0),
      new ZkContractNodeStub(2),
      new ZkContractNodeStub(3),
      new ZkContractNodeStub(1),
    ];

    const realContractNodes: RealContractNodes = new RealContractNodes(
      nodes,
      new TimeSleeper(sleepFun),
      1
    );

    await expect(() =>
      realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, BLINDED_SHARES)
    ).rejects.toEqual(new Error("Unable to send shares to nodes"));

    expect(sleepCalled).toEqual([]);

    expect(nodes[0].getSendCount()).toEqual(1);
    expect(nodes[1].getSendCount()).toEqual(1);
    expect(nodes[2].getSendCount()).toEqual(1);
    expect(nodes[3].getSendCount()).toEqual(1);
  });

  test("send shares with retries and higher number of retries", async () => {
    const sleepCalled: number[] = [];

    const sleepFun: (n: number) => Promise<void> = (n: number) => {
      sleepCalled.push(n);
      return new Promise((resolve) => resolve());
    };

    const nodes = [
      new ZkContractNodeStub(4),
      new ZkContractNodeStub(3),
      new ZkContractNodeStub(2),
      new ZkContractNodeStub(0),
    ];

    const realContractNodes: RealContractNodes = new RealContractNodes(
      nodes,
      new TimeSleeper(sleepFun),
      5
    );

    await realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, BLINDED_SHARES);

    expect(sleepCalled).toEqual([5000, 10000, 15000, 20000]);

    expect(nodes[0].getSendCount()).toEqual(5);
    expect(nodes[1].getSendCount()).toEqual(4);
    expect(nodes[2].getSendCount()).toEqual(3);
    expect(nodes[3].getSendCount()).toEqual(1);
  });

  /** If a single node fails always, then the call still succeeds with maximum sleeping. */
  test("send shares with retries one failure", async () => {
    const sleepCalled: number[] = [];

    const sleepFun: (n: number) => Promise<void> = (n: number) => {
      sleepCalled.push(n);
      return new Promise((resolve) => resolve());
    };

    const nodes = [
      new ZkContractNodeStub(0),
      new ZkContractNodeStub(10),
      new ZkContractNodeStub(0),
      new ZkContractNodeStub(0),
    ];

    const realContractNodes: RealContractNodes = new RealContractNodes(
      nodes,
      new TimeSleeper(sleepFun)
    );

    await realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, BLINDED_SHARES);

    expect(sleepCalled).toEqual([5000, 10000, 15000]);
    expect(nodes[0].getSendCount()).toEqual(1);
    expect(nodes[1].getSendCount()).toEqual(4);
    expect(nodes[2].getSendCount()).toEqual(1);
    expect(nodes[3].getSendCount()).toEqual(1);
  });

  /** If two nodes fails always, then the call fails with maximum sleeping. */
  test("send shares with retries two failures", async () => {
    const sleepCalled: number[] = [];

    const sleepFun: (n: number) => Promise<void> = (n: number) => {
      sleepCalled.push(n);
      return new Promise((resolve) => resolve());
    };

    const nodes = [
      new ZkContractNodeStub(0),
      new ZkContractNodeStub(10),
      new ZkContractNodeStub(0),
      new ZkContractNodeStub(10),
    ];

    const realContractNodes: RealContractNodes = new RealContractNodes(
      nodes,
      new TimeSleeper(sleepFun)
    );

    await expect(() =>
      realContractNodes.sendSharesToNodes(TRANSACTION_HASH, SENDER, BLINDED_SHARES)
    ).rejects.toEqual(new Error("Unable to send shares to nodes"));

    expect(sleepCalled).toEqual([5000, 10000, 15000]);
    expect(nodes[0].getSendCount()).toEqual(1);
    expect(nodes[1].getSendCount()).toEqual(4);
    expect(nodes[2].getSendCount()).toEqual(1);
    expect(nodes[3].getSendCount()).toEqual(4);
  });

  test("test setTimeout", async () => {
    const timeNow = new Date().getTime();
    await timeout(15000);
    const timeAfter = new Date().getTime();

    expect(14900 <= timeAfter - timeNow && timeAfter - timeNow <= 15100).toBeTruthy();
  });
});
