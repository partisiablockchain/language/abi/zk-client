/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BinaryExtensionFieldElement } from "../../main";

describe("BinaryExtensionFieldElement tests", () => {
  test("isOne", () => {
    expect(BinaryExtensionFieldElement.ONE.isOne()).toBe(true);
    expect(BinaryExtensionFieldElement.ZERO.isOne()).toBe(false);
    expect(BinaryExtensionFieldElement.createElement(14).isOne()).toBe(false);
  });

  test("isZero", () => {
    expect(BinaryExtensionFieldElement.ZERO.isZero()).toBe(true);
    expect(BinaryExtensionFieldElement.ONE.isZero()).toBe(false);
    expect(BinaryExtensionFieldElement.createElement(14).isZero()).toBe(false);
  });

  test("negate", () => {
    for (let i = 0; i < 16; i++) {
      const element = BinaryExtensionFieldElement.createElement(i);
      const negated = element.negate();

      expect(element.isEqualTo(negated)).toBe(true);
    }
  });

  test("modInverse", () => {
    const inverse = BinaryExtensionFieldElement.createElement(2).modInverse();
    expect(inverse.isEqualTo(BinaryExtensionFieldElement.createElement(9))).toBe(true);
  });

  test("square root", () => {
    for (let i = 0; i < 16; i++) {
      const x = BinaryExtensionFieldElement.createElement(i);
      const mul = x.multiply(x);
      const sqrt = mul.squareRoot();

      expect(x.isEqualTo(sqrt)).toBe(true);
    }
  });

  test("computation alphas", () => {
    expect(BinaryExtensionFieldElement.computationAlphas().length).toEqual(4);
    expect(
      BinaryExtensionFieldElement.computationAlphas()[0].isEqualTo(
        BinaryExtensionFieldElement.createElement(1)
      )
    );
    expect(
      BinaryExtensionFieldElement.computationAlphas()[1].isEqualTo(
        BinaryExtensionFieldElement.createElement(2)
      )
    );
    expect(
      BinaryExtensionFieldElement.computationAlphas()[2].isEqualTo(
        BinaryExtensionFieldElement.createElement(3)
      )
    );
    expect(
      BinaryExtensionFieldElement.computationAlphas()[3].isEqualTo(
        BinaryExtensionFieldElement.createElement(4)
      )
    );
  });
});
