package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.language.zkclient.shares.BinarySecretShares;
import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Tests on-chain methods of ZkRpcBuilder. */
public final class ZkRpcBuilderOnChainTest {
  /// Parameters used to generate the reference rpc
  private static final int REFERENCE_SECRET = 13;
  private static final byte REFERENCE_INVOCATION = 0x40;
  private static final String REFERENCE_SENDER = "bb";
  private static final String REFERENCE_ENCRYPTION_SECRET_KEY = "beedad";
  private static final long REFERENCE_SEED = 12345;

  /// Testnet engines (as base64 string)
  private static final String ENGINE_1 = "Ax3kZlMV9JW6EE/74YO9X8Y7zVeD8TubNlBaY+IMfARg";
  private static final String ENGINE_2 = "Awndg7zaEkVJxHgNzaITUUX+uzAXYl9Ja9X8QPcRy9pZ";
  private static final String ENGINE_3 = "A9SqNrfygSuXOLNsdy4Gx8d0kSV5S/ET7GCnTVz90FQ7";
  private static final String ENGINE_4 = "Ari1IPP44JCkTfB98nVyMn0y2bZwgkQEEvrBV/umVkUo";

  private byte[] referenceRpc;
  private BlockchainAddress referenceSender;
  private BinarySecretShares referenceInput;
  private byte[] referenceAdditionalRpc;
  private List<BlockchainPublicKey> referenceEngineKeys;
  private KeyPair referenceEncryptionKey;

  @BeforeEach
  void referenceConstants() {
    referenceRpc =
        Hex.decode(
            "05000000010000002002a755b11067317347cb614f1879b0b9"
                + "339adb5d1bf1b8694611707b176d1a82630000010090df042b"
                + "000738a6911e415984a2b6db005e773aebf2e1c278dae8fad0"
                + "b30188dc5d0d5646c8d3f52eda168de28858d3aac375495d26"
                + "865931ed8ce0849d457643fc7e627274d062fbcb9853642821"
                + "c4f0b56b6691dbd5bac47eb0c70e377d1b833168d61bd937df"
                + "7d2a484cc846aef9372f0a2e1634bb68ca03c46821918fa095"
                + "724bd0a3fd90ef13b2ca0d95a59012f7b2c5c14d913a483cc9"
                + "64823aead46937a19084732f2fe6fea81d2b8292f6f74d328f"
                + "1c53c9c53985752ce0810690d9dd476ddba9aae893fbb8cfb4"
                + "93aa9410fc2d10667d29d817eb85ca05d59e6cdc515e9ffd49"
                + "514c7210d47c7cb35590ec26279b4bdb05332aee69192647a0"
                + "3d6640");

    KeyPair senderKeys = new KeyPair(new BigInteger(REFERENCE_SENDER, 16));
    referenceSender = senderKeys.getPublic().createAddress();

    CompactBitArray input = BitOutput.serializeBits(s -> s.writeSignedInt(REFERENCE_SECRET, 32));
    referenceInput = new BinarySecretShares(input, referenceRng());
    referenceAdditionalRpc = new byte[] {REFERENCE_INVOCATION};

    // The list of the engine keys matter for the generated rpc
    referenceEngineKeys =
        List.of(
            BlockchainPublicKey.fromEncodedEcPoint(Base64.decode(ENGINE_2)),
            BlockchainPublicKey.fromEncodedEcPoint(Base64.decode(ENGINE_4)),
            BlockchainPublicKey.fromEncodedEcPoint(Base64.decode(ENGINE_3)),
            BlockchainPublicKey.fromEncodedEcPoint(Base64.decode(ENGINE_1)));

    referenceEncryptionKey = new KeyPair(new BigInteger(REFERENCE_ENCRYPTION_SECRET_KEY, 16));
  }

  private Random referenceRng() {
    return new Random(REFERENCE_SEED);
  }

  /** Test that a new rpc matches a reference rpc that has been tested on the testnet. */
  @Test
  public void rpcMatchesReference() {
    byte[] generatedRpc =
        ZkRpcBuilder.zkInputOnChain(
            referenceSender,
            referenceInput,
            referenceAdditionalRpc,
            referenceEngineKeys,
            referenceEncryptionKey);

    assertThat(generatedRpc).isEqualTo(referenceRpc);
  }

  /** Test that varying the parameters of the rpc builder generates different rpcs. */
  @Test
  public void differentRpcForDifferentInput() {
    byte[] someZkRpc =
        ZkRpcBuilder.zkInputOnChain(
            referenceSender,
            referenceInput,
            referenceAdditionalRpc,
            referenceEngineKeys,
            referenceEncryptionKey);

    KeyPair differentKey = new KeyPair();
    byte[] withDifferentSender =
        ZkRpcBuilder.zkInputOnChain(
            differentKey.getPublic().createAddress(),
            referenceInput,
            referenceAdditionalRpc,
            referenceEngineKeys,
            referenceEncryptionKey);
    assertThat(someZkRpc).isNotEqualTo(withDifferentSender);

    int newInt = 41;
    CompactBitArray differentSecret = BitOutput.serializeBits(s -> s.writeSignedInt(newInt, 32));
    BinarySecretShares differentInput = new BinarySecretShares(differentSecret, referenceRng());

    byte[] withDifferentSecret =
        ZkRpcBuilder.zkInputOnChain(
            referenceSender,
            differentInput,
            referenceAdditionalRpc,
            referenceEngineKeys,
            referenceEncryptionKey);
    assertThat(someZkRpc).isNotEqualTo(withDifferentSecret);

    byte differentShortname = 0x41;
    byte[] differentAdditionalRpc = new byte[] {differentShortname};

    byte[] withDifferentInvocation =
        ZkRpcBuilder.zkInputOnChain(
            referenceSender,
            referenceInput,
            differentAdditionalRpc,
            referenceEngineKeys,
            referenceEncryptionKey);
    assertThat(someZkRpc).isNotEqualTo(withDifferentInvocation);

    ArrayList<BlockchainPublicKey> differentEngines = new ArrayList<>(referenceEngineKeys);
    differentEngines.remove(0);
    KeyPair newKey = new KeyPair();
    differentEngines.add(0, newKey.getPublic());
    byte[] withDifferentEngineKeys =
        ZkRpcBuilder.zkInputOnChain(
            referenceSender,
            referenceInput,
            referenceAdditionalRpc,
            differentEngines,
            referenceEncryptionKey);
    assertThat(someZkRpc).isNotEqualTo(withDifferentEngineKeys);

    byte[] withDifferentEncryptionKey =
        ZkRpcBuilder.zkInputOnChain(
            referenceSender,
            referenceInput,
            referenceAdditionalRpc,
            referenceEngineKeys,
            differentKey);
    assertThat(someZkRpc).isNotEqualTo(withDifferentEncryptionKey);
  }

  /** Test that rpc's created using ephemeral keys are different for same input. */
  @Test
  public void differentRpcForSameInputWhenUsingPublicMethod() {
    int secretInt = 42;
    CompactBitArray binaryInput =
        BitOutput.serializeBits(output -> output.writeSignedInt(secretInt, 32));
    BinarySecretShares binarySecretShares = new BinarySecretShares(binaryInput, referenceRng());

    byte[] someZkRpc =
        ZkRpcBuilder.zkInputOnChain(
            referenceSender, binarySecretShares, referenceAdditionalRpc, referenceEngineKeys);
    byte[] differentZkRpc =
        ZkRpcBuilder.zkInputOnChain(
            referenceSender, binarySecretShares, referenceAdditionalRpc, referenceEngineKeys);
    assertThat(someZkRpc).isNotEqualTo(differentZkRpc);
  }
}
