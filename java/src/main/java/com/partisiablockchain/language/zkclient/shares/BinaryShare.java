package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Secret shares are disjoint parts of a secret input. A secret share consists of a list of
 * BinaryExtensionFieldElements which are in turn secret shares of a single bit.
 */
public final class BinaryShare implements SecretShare {

  final List<BinaryExtensionFieldElement> bitElements;

  /**
   * Constructs a secret share from a list of bit elements.
   *
   * @param bitElements the bit elements that constitutes the share
   */
  public BinaryShare(List<BinaryExtensionFieldElement> bitElements) {
    this.bitElements = bitElements;
  }

  /**
   * Read a binary share from a stream, in the format specified by the ZK nodes. This is used when
   * shares are fetched from ZK nodes during reconstruction of a secret variable.
   *
   * @param stream the stream to read the share from
   * @return the read share
   */
  public static BinaryShare read(SafeDataInputStream stream) {
    int numOfElements = stream.readInt();
    List<BinaryExtensionFieldElement> sharesOfBits = new ArrayList<>();
    for (int i = 0; i < numOfElements; i++) {
      int bitLength = stream.readInt();
      for (int j = 0; j < bitLength; j++) {
        BinaryExtensionFieldElement shareOfBit =
            BinaryExtensionFieldElement.create(stream.readSignedByte());
        sharesOfBits.add(shareOfBit);
      }
    }
    return new BinaryShare(sharesOfBits);
  }

  /**
   * Encrypts a share using a shared key. The shared key is created from an ephemeral (temporary)
   * private key and the public key of the receiving party. This allows only the receiving party to
   * recover the value of the share. The private key is ephemeral such that two identical shares
   * will be encrypted differently in order to prevent replay attacks. The identity (address) of the
   * sender is encrypted along with the share such that a third party cannot spoof the identity of
   * the sender.
   *
   * @param ephemeralKey The temporary key to encrypt the share with
   * @param sender the address of the sender of the secret input
   * @param enginePublicKey the key of the engine that is to receive the encrypted share
   * @return the encrypted share
   */
  public EncryptedBinaryShares.Share encrypt(
      KeyPair ephemeralKey, BlockchainAddress sender, BlockchainPublicKey enginePublicKey) {

    Hash sharedKey = enginePublicKey.deriveSharedKey(ephemeralKey.getPrivateKey());
    Cipher encryptionCipher = createEncryptionCipher(sharedKey);
    byte[] shareAndSenderAddress = serializeWithSenderAddress(sender);
    return new EncryptedBinaryShares.Share(
        ExceptionConverter.call(() -> encryptionCipher.doFinal(shareAndSenderAddress)));
  }

  /**
   * Serializes this share along with the address of the sender of the input.
   *
   * @param sender the sender of the secret input this share was created from
   * @return the serialized share and address
   */
  private byte[] serializeWithSenderAddress(BlockchainAddress sender) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.write(this.serialize());
          sender.write(stream);
        });
  }

  /**
   * Applies the supplied blinding by serializing this share along with the blinding.
   *
   * @param blinding the blinding to apply
   * @return the serialized share and blinding
   */
  BlindedBinaryShares.Share serializeWithBlinding(Blindings.Blinding blinding) {
    return new BlindedBinaryShares.Share(
        SafeDataOutputStream.serialize(
            stream -> {
              stream.write(this.serialize());
              stream.write(blinding.blindingBytes());
            }));
  }

  /**
   * Serializes each bit element of the share.
   *
   * @return the serialized share
   */
  private byte[] serialize() {
    return SafeDataOutputStream.serialize(
        stream -> {
          for (BinaryExtensionFieldElement bitElement : bitElements) {
            stream.write(bitElement.serialize());
          }
        });
  }

  /**
   * Creates an AES encryption cipher from a shared key. The shared key is generated from an
   * ephemeral (temporary) private key, and the public key of the receiver of the encrypted message.
   *
   * @param sharedKey the shared key to generate the cipher from
   * @return the AES cipher
   */
  static Cipher createEncryptionCipher(Hash sharedKey) {
    return ExceptionConverter.call(
        () -> {
          Cipher aes = Cipher.getInstance("AES/CBC/PKCS5Padding");
          IvParameterSpec ivParameterSpec =
              new IvParameterSpec(Arrays.copyOfRange(sharedKey.getBytes(), 0, 16));
          SecretKeySpec secretKeySpec =
              new SecretKeySpec(Arrays.copyOfRange(sharedKey.getBytes(), 16, 32), "AES");
          aes.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
          return aes;
        },
        "Unable to initialize AES");
  }
}
