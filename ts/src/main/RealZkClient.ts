/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  BlockchainAddress,
  Signature,
  Transaction,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { BlindedBinaryShares } from "./BlindedBinaryShares";
import { CompactBitArray } from "@secata-public/bitmanipulation-ts";
import { ec } from "elliptic";
import { Client } from "./util/Client";
import { randomBytes } from "crypto";
import { BinarySecretShares } from "./BinarySecretShares";
import { CryptoUtils } from "./util/CryptoUtils";
import { OffChainInputRpc, ZkRpcBuilder } from "./ZkRpcBuilder";
import { ZkContractNodes } from "./node/ZkContractNodes";
import { RealZkNodes } from "./node/RealZkNodes";

import KeyPair = ec.KeyPair;
import { SignatureProvider } from "./util/SignatureProvider";

export class RealZkClient {
  readonly rng: (ln: number) => Buffer;
  readonly realNodes: RealZkNodes;
  readonly zkContractAddress: BlockchainAddress;
  readonly blockchainClient: Client;
  cachedZkContractNodes: ZkContractNodes | undefined;

  constructor(
    zkContractAddress: BlockchainAddress,
    realNodes: RealZkNodes,
    rng: (ln: number) => Buffer,
    blockchainClient: Client
  ) {
    this.zkContractAddress = zkContractAddress;
    this.realNodes = realNodes;
    this.rng = rng;
    this.blockchainClient = blockchainClient;
  }

  async getZkContractNodes(): Promise<ZkContractNodes> {
    if (this.cachedZkContractNodes == null) {
      this.cachedZkContractNodes = await this.realNodes.getNodes(this.zkContractAddress);
    }

    return this.cachedZkContractNodes;
  }

  public static create(contract: BlockchainAddress, blockchainClient: Client): RealZkClient {
    const realNodes: RealZkNodes = RealZkNodes.create(blockchainClient);
    return new RealZkClient(contract, realNodes, randomBytes, blockchainClient);
  }

  public static async createForTest(
    contract: BlockchainAddress,
    blockchainClient: Client,
    rng: (ln: number) => Buffer
  ): Promise<RealZkClient> {
    const realNodes: RealZkNodes = RealZkNodes.create(blockchainClient);
    return new RealZkClient(contract, realNodes, rng, blockchainClient);
  }

  public async buildOnChainInputTransaction(
    sender: BlockchainAddress,
    secretInput: CompactBitArray,
    publicRpc: Buffer
  ): Promise<Transaction> {
    const publicKeysOfNodes = (await this.getZkContractNodes()).getPublicKeysOfNodes();
    const key: KeyPair = CryptoUtils.generateKeyPair();
    const rpc = ZkRpcBuilder.zkInputOnChain(sender, secretInput, publicRpc, publicKeysOfNodes, key);

    return { address: this.zkContractAddress, rpc };
  }

  buildOffChainInputTransaction(secretInput: CompactBitArray, publicRpc: Buffer): OffChainInput {
    const offChainInputRpc: OffChainInputRpc = ZkRpcBuilder.zkInputOffChain(
      secretInput,
      publicRpc,
      this.rng
    );
    const transaction: Transaction = {
      address: this.zkContractAddress,
      rpc: offChainInputRpc.rpc,
    };

    return { blindedShares: offChainInputRpc.blindedShares, transaction };
  }

  async sendOffChainInputToNodes(
    zkContract: BlockchainAddress,
    sender: BlockchainAddress,
    onChainTransaction: string,
    shares: BlindedBinaryShares
  ): Promise<void> {
    return (await this.getZkContractNodes()).sendSharesToNodes(onChainTransaction, sender, shares);
  }

  async fetchSecretVariable(
    owner: SignatureProvider,
    variableId: number
  ): Promise<CompactBitArray> {
    const zkContractNodes = await this.getZkContractNodes();
    const secretShares: BinarySecretShares = await zkContractNodes.fetchSharesFromNodes(
      owner,
      variableId
    );
    return secretShares.reconstructSecret();
  }

  async getAttestationWithSignatures(
    contractAddress: BlockchainAddress,
    attestationId: number
  ): Promise<Attestation> {
    const zkContract = await this.blockchainClient.getContractState(contractAddress);

    if (zkContract === undefined) {
      throw new Error("Couldn't get contract data");
    }
    const attestations = zkContract.serializedContract.attestations;

    for (let i = 0; i < attestations.length; i++) {
      const element = attestations[i];
      if (element.key == attestationId) {
        return {
          data: Buffer.from(element.value.data.data, "base64"),
          signatures: element.value.signatures,
        };
      }
    }
    throw new Error("Unable to get signatures for the attestation with id " + attestationId);
  }
}

export interface Attestation {
  data: Buffer;
  signatures: Signature[];
}

export interface OffChainInput {
  blindedShares: BlindedBinaryShares;
  transaction: Transaction;
}
