package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import java.util.ArrayList;
import java.util.List;

/**
 * Blinded secret shares of binary data. When inputting a secret off-chain, blinded shares of the
 * secret are created, then hashed and put on the blockchain to commit to the value of the secret.
 * Afterwards, the blinded shares are sent directly and securely to the individual parties
 * (engines), who can then verify their value against the hashes on the blockchain. Without
 * blinding, for small input sizes, other parties than the one receiving the share could guess the
 * value of the share by trying all possible hashes.
 */
public final class BlindedBinaryShares {
  private final List<Share> shares;

  /**
   * Constructs blinded shares for a list of shares. The list contains a blinded share for each
   * receiving party of the secret input.
   *
   * @param shares the blinded shares
   */
  BlindedBinaryShares(List<Share> shares) {
    this.shares = shares;
  }

  /**
   * Hash each blinded share in order to conceal its value. The hashes are then used as commitments
   * to the values of the shares.
   *
   * @return ShareCommitments created from the hashes of the blinded shares
   */
  public ShareCommitments hash() {
    List<Hash> commitments = new ArrayList<>();
    for (Share blindedShare : shares) {
      Hash commitment = Hash.create(s -> s.write(blindedShare.blinded));
      commitments.add(commitment);
    }
    return new ShareCommitments(commitments);
  }

  /**
   * Returns the blinded shares.
   *
   * @return the shares
   */
  public List<Share> getShares() {
    return shares;
  }

  /**
   * A blinded secret share. Secret shares are disjoint parts of a secret input.
   *
   * @param blinded the serialized share
   */
  @SuppressWarnings("ArrayRecordComponent")
  public record Share(byte[] blinded) {}
}
