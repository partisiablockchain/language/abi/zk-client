package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;

/**
 * Share commitments are hashes of blinded shares, used to commit to the values of said shares, by
 * putting the hashes on the blockchain.
 */
public final class ShareCommitments {
  private final List<Hash> blindedHashedShares;

  /**
   * Constructs ShareCommitments from a list of hashes.
   *
   * @param blindedHashedShares hashes of the blinded shares to commit to
   */
  ShareCommitments(List<Hash> blindedHashedShares) {
    this.blindedHashedShares = blindedHashedShares;
  }

  /**
   * Serializes each blinded hashed share.
   *
   * @return the serialized share commitments
   */
  public byte[] serialize() {
    return SafeDataOutputStream.serialize(
        stream -> {
          for (Hash hash : blindedHashedShares) {
            hash.write(stream);
          }
        });
  }
}
