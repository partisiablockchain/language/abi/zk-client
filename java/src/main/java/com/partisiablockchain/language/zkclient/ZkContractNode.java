package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;

/** A single computation engine running a ZK computation. */
public interface ZkContractNode {

  /**
   * Send a secret share to this node.
   *
   * @param account the input sender.
   * @param inputTransaction pointer to on-chain transaction, used for authentication of the share.
   * @param share the first share for this engine.
   */
  void sendShareToNode(BlockchainAddress account, Hash inputTransaction, byte[] share);

  /**
   * Get the share held by the Zk Node for a given variable, only the owner of the variable is
   * allowed to fetch the variable.
   *
   * @param signature the signature of the fetch request.
   * @param variableId the id of variable.
   * @return the bytes of the share.
   */
  byte[] fetchShareFromNode(Signature signature, int variableId);

  /**
   * Get the public key for the node.
   *
   * @return the public key of the node.
   */
  BlockchainPublicKey getPublicKey();

  /**
   * Get the rest interface url of the node.
   *
   * @return the rest interface url of the node.
   */
  String getRestInterface();

  /**
   * Create Hash used for requesting secret shared variables from the node. This hash is used to
   * create a signature, which is required to fetch a share of the variable from the node (see
   * {@link ZkContractNodes#createFetchSignatures}).
   *
   * <p>This hash contains:
   *
   * <ol>
   *   <li>variable header (the value "shamirvariable")
   *   <li>the blockchain address of the node
   *   <li>the contract address
   *   <li>the variable id
   * </ol>
   *
   * @param variableId the id of variable.
   * @return the hash of a variable fetch request.
   */
  Hash hashRequest(int variableId);
}
