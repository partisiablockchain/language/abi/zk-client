/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BinarySecretShares } from "../BinarySecretShares";
import { BlockchainAddress } from "@partisiablockchain/blockchain-api-transaction-client";
import { BlockchainPublicKey } from "../util/BlockchainPublicKey";
import { BlindedBinaryShares } from "../BlindedBinaryShares";
import { SignatureProvider } from "../util/SignatureProvider";

/** Zk nodes allocated to a specific Zk contract. Used to send and fetch shares from the nodes. */
export interface ZkContractNodes {
  /**
   * Fetch a secret variable from the Zk nodes associated to a specific contract. The secret must be
   * owned by the account signing the request.
   *
   * @param owner the authentication used to prove ownership of variable.
   * @param variableId the id of the variable to fetch.
   * @return the binary secret shares for the given variable.
   */
  fetchSharesFromNodes(owner: SignatureProvider, variableId: number): Promise<BinarySecretShares>;

  /**
   * Sends blinded secret shares to the ZK nodes over https.
   *
   * @param transactionsHash the verification transaction the nodes can verify the share they receive
   *     with.
   * @param sender the sender of the off-chain input
   * @param blindedShares the blinded shares to send to the nodes.
   */
  sendSharesToNodes(
    transactionsHash: string,
    sender: BlockchainAddress,
    blindedShares: BlindedBinaryShares
  ): Promise<void>;

  /**
   * Get the public keys of the nodes.
   *
   * @return the list of public keys.
   */
  getPublicKeysOfNodes(): BlockchainPublicKey[];
}
