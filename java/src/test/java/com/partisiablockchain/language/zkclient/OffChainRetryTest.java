package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.abicodegen.AverageSalary;
import com.partisiablockchain.language.abicodegen.RealDeploy;
import com.partisiablockchain.language.recordreplay.BlockchainRecordReplay;
import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

final class OffChainRetryTest {

  private static final boolean RECORDING = false;
  private static final String REFERENCE_FOLDER = "src/test/resources/zkclient/";
  private static final String TESTNET = "https://node1.testnet.partisiablockchain.com";
  private static final BlockchainAddress REAL_DEPLOY =
      BlockchainAddress.fromString("018bc1ccbb672b87710327713c97d43204905082cb");

  private static final SenderAuthenticationKeyPair OWNER_KEY =
      SenderAuthenticationKeyPair.fromString("aa");
  private static final SenderAuthenticationKeyPair USER1_KEY =
      SenderAuthenticationKeyPair.fromString("bb");
  private static BlockchainTransactionClient USER1;
  private static BlockchainAddress contractAddress;
  private static RealZkClient zkClient;
  private BlockchainClient client;

  @BeforeAll
  static void deployZk(TestInfo info) {

    BlockchainRecordReplay recordReplay =
        new BlockchainRecordReplay(RECORDING, REFERENCE_FOLDER + info.getDisplayName(), TESTNET, 3);

    BlockchainTransactionClient deployClient = recordReplay.getTransactionClient(OWNER_KEY);

    byte[] initAverageSalary = AverageSalary.initialize();
    byte[] averageSalaryZkwa =
        ExceptionConverter.call(
            () -> Files.readAllBytes(Path.of("src/test/resources/contract/average_salary.zkwa")));
    byte[] averageSalaryAbi =
        ExceptionConverter.call(
            () -> Files.readAllBytes(Path.of("src/test/resources/contract/average_salary.abi")));
    byte[] deploymentBytes =
        RealDeploy.deployContractV3(
            averageSalaryZkwa, initAverageSalary, averageSalaryAbi, 20000000, List.of(), 6);

    Transaction deployTransaction = Transaction.create(REAL_DEPLOY, deploymentBytes);

    SentTransaction sentTransaction = deployClient.signAndSend(deployTransaction, 6000000);
    deployClient.waitForSpawnedEvents(sentTransaction);
    contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_ZK, sentTransaction.signedTransaction().identifier());
  }

  @BeforeEach
  void setup(TestInfo info) {
    BlockchainRecordReplay recordReplay =
        new BlockchainRecordReplay(RECORDING, REFERENCE_FOLDER + info.getDisplayName(), TESTNET, 3);

    Random rng = new Random(1234L);
    zkClient =
        RealZkClient.createForTest(
            contractAddress, recordReplay.getWebClient(), recordReplay.getBlockchainClient(), rng);

    USER1 = recordReplay.getTransactionClient(USER1_KEY);

    client = recordReplay.getBlockchainClient();
  }

  /** Sending a secret input to a Zk contract off-chain with retries. */
  @Test
  void sendSecretInputOffChainWithRetries() throws InterruptedException {
    RealZkClient.OffChainInput offChainInput =
        zkClient.buildOffChainInputTransaction(
            createSecretIntInput(10000), averageSalaryPublicRpc());
    SentTransaction offChainInputTransaction =
        USER1.signAndSend(offChainInput.transaction(), 20000);

    USER1.waitForSpawnedEvents(offChainInputTransaction);

    Hash identifier = offChainInputTransaction.signedTransaction().identifier();
    zkClient.sendOffChainInputToNodes(
        contractAddress, USER1_KEY.getAddress(), identifier, offChainInput.shares());

    if (RECORDING) {
      Thread.sleep(10000);
    }
    JsonNode state = client.getContractState(contractAddress).serializedContract();
    Assertions.assertThat(TestNetRealZkClientTest.isInputReceived(state, identifier)).isTrue();
  }

  private byte[] averageSalaryPublicRpc() {
    return new byte[] {0x40};
  }

  CompactBitArray createSecretIntInput(int secret) {
    return BitOutput.serializeBits(output -> output.writeSignedInt(secret, 32));
  }
}
