package com.partisiablockchain.language.zkclient.shares;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.stream.BitInput;
import com.secata.stream.BitOutput;
import com.secata.stream.CompactBitArray;
import org.junit.jupiter.api.Test;

/** Tests methods for reconstructing secret variable data. */
public final class ShareAndReconstructTest {
  @Test
  void shareAndReconstructInt() {
    int secret = 55;

    CompactBitArray secretVarData = BinarySecretSharesTest.secretVarDataFromInt(secret);
    BinarySecretShares secretShares = new BinarySecretShares(secretVarData);

    CompactBitArray reconstructedSecret = secretShares.reconstructSecret();
    int converted = BitInput.create(reconstructedSecret.data()).readSignedInt(32);
    assertThat(converted).isEqualTo(secret);
  }

  @Test
  void shareAndReconstructLong() {
    long secret = 123456789L;
    int bitLength = 64;
    CompactBitArray secretVarData =
        BitOutput.serializeBits(output -> output.writeSignedLong(secret, bitLength));

    BinarySecretShares secretShares = new BinarySecretShares(secretVarData);

    CompactBitArray reconstructedSecret = secretShares.reconstructSecret();
    long converted = BitInput.create(reconstructedSecret.data()).readSignedLong(64);

    assertThat(converted).isEqualTo(secret);
  }
}
