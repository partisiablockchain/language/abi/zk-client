/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ZkContractNode } from "./ZkContractNode";
import { BlockchainAddress } from "@partisiablockchain/blockchain-api-transaction-client";
import { BlockchainPublicKey } from "../util/BlockchainPublicKey";
import { BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { postRequest, putRequest } from "../util/Api";

export class RealContractNode implements ZkContractNode {
  private static readonly VARIABLE_HEADER: number[] = [
    "s",
    "h",
    "a",
    "m",
    "i",
    "r",
    "v",
    "a",
    "r",
    "i",
    "a",
    "b",
    "l",
    "e",
  ].map((s) => s.charCodeAt(0));

  readonly contract: BlockchainAddress;
  readonly identity: string;
  readonly publicKey: string;
  readonly restInterface: string;

  constructor(
    contract: BlockchainAddress,
    nodeAddress: string,
    publicKey: string,
    restInterface: string
  ) {
    this.contract = contract;
    this.identity = nodeAddress;
    this.publicKey = publicKey;
    this.restInterface = restInterface;
  }

  async fetchSharesFromNode(signedMessage: Buffer, variableId: number): Promise<Buffer> {
    const url: string = this.outputUrl(variableId);

    const response: OutputDto | undefined = await postRequest(
      url,
      createSignatureDto(signedMessage.toString("base64"))
    );

    if (response === undefined) {
      throw new Error("Result when fetching shares is undefined");
    }

    return Buffer.from(response.data, "base64");
  }

  getPublicKey(): BlockchainPublicKey {
    const val = Buffer.from(this.publicKey, "base64");
    return BlockchainPublicKey.fromBuffer(val);
  }

  sendShareToNode(
    account: BlockchainAddress,
    inputTransaction: string,
    share: Buffer
  ): Promise<boolean> {
    const url: string = this.inputUrl(this.contract, account, inputTransaction);
    const inputDto: InputDto = { data: share.toString("base64") };

    return putRequest(url, inputDto);
  }

  createMessage(variableId: number): Buffer {
    return BigEndianByteOutput.serialize((out) => {
      out.writeBytes(Buffer.from(RealContractNode.VARIABLE_HEADER));
      out.writeBytes(Buffer.from(this.identity, "hex"));
      out.writeBytes(Buffer.from(this.contract, "hex"));
      out.writeI32(variableId);
    });
  }
  outputUrl(variableId: number): string {
    return `${this.restInterface}/real/binary/output/variable/${this.contract}/${variableId}`;
  }

  inputUrl(
    contractAddress: BlockchainAddress,
    accountAddress: BlockchainAddress,
    transactionHash: string
  ): string {
    return `${this.restInterface}/real/binary/input/${contractAddress}/${accountAddress}/${transactionHash}`;
  }
}

export interface SignatureDto {
  readonly signature: Buffer;
}

function createSignatureDto(signature: string): { signature: string } {
  return { signature };
}

export interface OutputDto {
  data: string;
}

export interface InputDto {
  data: string;
}
