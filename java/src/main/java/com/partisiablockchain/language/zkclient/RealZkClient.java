package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.language.zkclient.node.RealZkNodes;
import com.partisiablockchain.language.zkclient.shares.BinarySecretShares;
import com.partisiablockchain.language.zkclient.shares.BlindedBinaryShares;
import com.secata.stream.CompactBitArray;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bouncycastle.util.encoders.Base64;

/** Send and get secret variables to a Zk contract with a given Blockchain account. */
public final class RealZkClient {

  private final Random rng;
  private final BlockchainAddress zkContractAddress;
  private final BlockchainClient blockchainClient;
  private final RealZkNodes realNodes;
  private ZkContractNodes cachedZkContractNodes;

  private RealZkClient(
      BlockchainAddress zkContractAddress,
      RealZkNodes realNodes,
      Random rng,
      BlockchainClient blockchainClient) {
    this.zkContractAddress = zkContractAddress;
    this.realNodes = realNodes;
    this.rng = rng;
    this.blockchainClient = blockchainClient;
  }

  private ZkContractNodes getZkContractNodes() {
    if (cachedZkContractNodes == null) {
      cachedZkContractNodes = realNodes.getNodes(zkContractAddress);
    }

    return cachedZkContractNodes;
  }

  /**
   * Create a REAL ZK client, to send inputs and fetch secrets from the zk nodes.
   *
   * @param contract the address of the deployed REAL contract.
   * @param webClient the webclient used for node communication.
   * @param blockchainClient the client used for chain interactions.
   * @return the zk client.
   */
  public static RealZkClient create(
      BlockchainAddress contract, WebClient webClient, BlockchainClient blockchainClient) {
    RealZkNodes realNodes = RealZkNodes.create(webClient, blockchainClient);
    return new RealZkClient(contract, realNodes, new SecureRandom(), blockchainClient);
  }

  /**
   * Create a client with a given randomness, should only be used for testing.
   *
   * @param webClient the webclient used for node communication.
   * @param blockchainClient the client used for chain interactions.
   * @param rng the randomness to use.
   * @return the zk client with specified randomness.
   */
  static RealZkClient createForTest(
      BlockchainAddress contract,
      WebClient webClient,
      BlockchainClient blockchainClient,
      Random rng) {
    RealZkNodes realNodes = RealZkNodes.create(webClient, blockchainClient);
    return new RealZkClient(contract, realNodes, rng, blockchainClient);
  }

  /**
   * Build a transaction that contains the encrypted share for each Zk node and the public rpc for
   * the 'on secret input' function.
   *
   * @param sender the sender of the input.
   * @param secretInput the secret input to send.
   * @param publicRpc the public arguments for the on secret input function.
   * @return the transaction to sign and send to the chain.
   */
  public Transaction buildOnChainInputTransaction(
      BlockchainAddress sender, CompactBitArray secretInput, byte[] publicRpc) {

    List<BlockchainPublicKey> publicKeysOfNodes = getZkContractNodes().getPublicKeysOfNodes();

    BinarySecretShares secretShares = new BinarySecretShares(secretInput, rng);
    KeyPair key = new KeyPair(new BigInteger(Curve.CURVE.getN().bitLength(), rng));
    byte[] rpc =
        ZkRpcBuilder.zkInputOnChain(sender, secretShares, publicRpc, publicKeysOfNodes, key);

    return Transaction.create(zkContractAddress, rpc);
  }

  /**
   * Create the Secret Shares and the public rpc for an Off chain input to a given contract. Used
   * for sending an input directly to the nodes, and the verification transaction to send to the
   * chain.
   *
   * @param secretInput the secret input to send.
   * @param publicRpc the public arguments for the on secret input function.
   * @return the Off chain input that contains the shares to send directly to the nodes and the
   *     transaction to send put on chain for verification of the shares.
   */
  public OffChainInput buildOffChainInputTransaction(
      CompactBitArray secretInput, byte[] publicRpc) {

    BinarySecretShares secretShares = new BinarySecretShares(secretInput, rng);
    ZkRpcBuilder.OffChainInput offChainInput =
        ZkRpcBuilder.zkInputOffChain(secretShares, publicRpc);
    Transaction transaction = Transaction.create(zkContractAddress, offChainInput.rpc());

    return new OffChainInput(offChainInput.blindedBinaryShares(), transaction);
  }

  /**
   * Send shares directly to the nodes.
   *
   * @param zkContract the address of the contract the input is for.
   * @param sender the address of the sender of the input.
   * @param onChainTransaction the on chain transaction used for share verification.
   * @param shares the shares to send to the nodes.
   */
  public void sendOffChainInputToNodes(
      BlockchainAddress zkContract,
      BlockchainAddress sender,
      Hash onChainTransaction,
      BlindedBinaryShares shares) {
    getZkContractNodes().sendSharesToNodes(onChainTransaction, sender, zkContract, shares);
  }

  /**
   * Fetch the binary data for a secret variable. The secret can only be fetched by the owner of the
   * variable.
   *
   * @param senderAuthentication authentication used for signing requests for secret variable.
   * @param variableId the identifier of the secret variable.
   * @return the binary data for the secret variable.
   */
  public CompactBitArray fetchSecretVariable(
      SenderAuthentication senderAuthentication, int variableId) {

    BinarySecretShares secretShares =
        getZkContractNodes().fetchSharesFromNodes(senderAuthentication, variableId);
    return secretShares.reconstructSecret();
  }

  /**
   * Get rest interfaces for zk nodes.
   *
   * @return a list of rest interfaces for zk nodes.
   */
  public List<String> getNodeRestInterfaces() {
    return getZkContractNodes().getRestInterfacesOfNodes();
  }

  /**
   * Create signatures for fetching a secret shared variable from each node. These signatures are
   * required to fetch a share of the variable.
   *
   * @param senderAuthentication the authentication for the owner of the variable.
   * @param variableId the variable to fetch.
   * @return the list of fetch request signatures.
   */
  public List<Signature> getRequestSignatures(
      SenderAuthentication senderAuthentication, int variableId) {
    return getZkContractNodes().createFetchSignatures(senderAuthentication, variableId);
  }

  /**
   * Get the attestation data and the nodes' attestation signatures for a given attestation id in a
   * ZK contract.
   *
   * @param contractAddress the ZK contract with an attestation.
   * @param attestationId the id of the attestation whose signatures we want to fetch.
   * @return the list of attestation signatures and the signed data.
   */
  public Attestation getAttestationWithSignatures(
      BlockchainAddress contractAddress, int attestationId) {

    ContractState zkContract = blockchainClient.getContractState(contractAddress);
    JsonNode attestations = zkContract.serializedContract().get("attestations");

    for (int i = 0; i < attestations.size(); i++) {
      int key = attestations.get(i).get("key").asInt();
      if (key == attestationId) {
        String attestation = attestations.get(i).get("value").get("data").get("data").textValue();
        byte[] data = Base64.decode(attestation);
        JsonNode signatureList = attestations.get(i).get("value").get("signatures");
        List<Signature> signatures = new ArrayList<>();
        for (int j = 0; j < signatureList.size(); j++) {
          String signature = signatureList.get(j).textValue();
          signatures.add(Signature.fromString(signature));
        }
        return new Attestation(data, signatures);
      }
    }
    throw new RuntimeException(
        "Unable to get signatures for the attestation with id " + attestationId);
  }

  /**
   * The transaction used for verification when sending a secret input off chain to the nodes, and
   * the blinded shares to send to the nodes.
   *
   * @param shares the shares to send to the nodes directly.
   * @param transaction the transaction used for verification by the nodes.
   */
  public record OffChainInput(BlindedBinaryShares shares, Transaction transaction) {}

  /**
   * An attestation of some data together with the nodes' signatures.
   *
   * @param data the attestation data.
   * @param signatures the nodes' signatures of the attestation data.
   */
  @SuppressWarnings("ArrayRecordComponent")
  public record Attestation(byte[] data, List<Signature> signatures) {}
}
