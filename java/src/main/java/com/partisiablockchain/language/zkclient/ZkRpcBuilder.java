package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.language.zkclient.shares.BinarySecretShares;
import com.partisiablockchain.language.zkclient.shares.BlindedBinaryShares;
import com.partisiablockchain.language.zkclient.shares.EncryptedBinaryShares;
import com.partisiablockchain.language.zkclient.shares.ShareCommitments;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;

/** Contains methods for building ZK input rpc. */
public final class ZkRpcBuilder {

  private ZkRpcBuilder() {}

  /**
   * Invocation in real contract binder for creating ZK input off chain. <a
   * href="https://gitlab.com/partisiablockchain/language/real-wasm-binder/-/blob/main/src/main/java/com/partisiablockchain/zk/real/binder/RealContractBinder.java">Link
   * to binder</a>
   */
  private static final byte ZERO_KNOWLEDGE_INPUT_OFF_CHAIN = 0x04;

  /**
   * Invocation in real contract binder for creating ZK input on chain. <a
   * href="https://gitlab.com/partisiablockchain/language/real-wasm-binder/-/blob/main/src/main/java/com/partisiablockchain/zk/real/binder/RealContractBinder.java">Link
   * to binder</a>
   */
  private static final byte ZERO_KNOWLEDGE_INPUT_ON_CHAIN = 0x05;

  /**
   * Build the rpc for inputting secret binary data on-chain to a ZK contract, using an ephemeral
   * (temporary) key and a cryptographically secure randomness generator.
   *
   * @param sender of the input, which is encrypted along with the input data
   * @param secretShares the binary shares to create rpc from
   * @param additionalRpc additional rpc to send along with transaction. Must as a minimum include
   *     the zk input shortname
   * @param engineKeys the public keys of the ZK engines associated with the ZK contract
   * @return the rpc
   */
  public static byte[] zkInputOnChain(
      BlockchainAddress sender,
      BinarySecretShares secretShares,
      byte[] additionalRpc,
      List<BlockchainPublicKey> engineKeys) {
    KeyPair ephemeralKey = new KeyPair();
    return zkInputOnChain(sender, secretShares, additionalRpc, engineKeys, ephemeralKey);
  }

  /**
   * Build the rpc for inputting secret binary data on-chain to a ZK contract, using a supplied key
   * pair and random number generator.
   *
   * @param sender the sender of the input, which is encrypted along with the input data
   * @param secretShares the binary shares to create rpc from
   * @param additionalRpc additional rpc to send along with transaction. Must as a minimum include
   *     the zk input shortname
   * @param engineKeys the public keys of the ZK engines associated with the ZK contract
   * @param encryptionKey the key pair used for encrypting the secret
   * @return the rpc
   */
  public static byte[] zkInputOnChain(
      BlockchainAddress sender,
      BinarySecretShares secretShares,
      byte[] additionalRpc,
      List<BlockchainPublicKey> engineKeys,
      KeyPair encryptionKey) {

    final EncryptedBinaryShares encryptedShares =
        secretShares.encrypt(encryptionKey, sender, engineKeys);
    final byte[] concatenatedEncryptedShares = encryptedShares.serialize();

    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ZERO_KNOWLEDGE_INPUT_ON_CHAIN);

          int[] bitLengths = new int[] {secretShares.getShareBitLength()};
          stream.writeInt(bitLengths.length);
          for (int bitLength : bitLengths) {
            stream.writeInt(bitLength);
          }

          encryptionKey.getPublic().write(stream);
          stream.writeDynamicBytes(concatenatedEncryptedShares);
          stream.write(additionalRpc);
        });
  }

  /**
   * Build the rpc for inputting secret binary data on-chain to a ZK contract, using a supplied
   * random number generator.
   *
   * @param secretShares the binary secret shares to create rpc from
   * @param additionalRpc additional rpc to send along with transaction. Must as a minimum include
   *     the zk input shortname
   * @return off-chain input consisting of a rpc to be sent to the real wasm binder and blinded
   *     shares to be sent to ZK nodes associated with the contract
   */
  public static OffChainInput zkInputOffChain(
      BinarySecretShares secretShares, byte[] additionalRpc) {
    final BlindedBinaryShares blindedBinaryShares = secretShares.applyBlinding();
    final ShareCommitments shareCommitments = blindedBinaryShares.hash();

    final byte[] rpc =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(ZERO_KNOWLEDGE_INPUT_OFF_CHAIN);

              int[] bitLengths = new int[] {secretShares.getShareBitLength()};
              stream.writeInt(bitLengths.length);
              for (int bitLength : bitLengths) {
                stream.writeInt(bitLength);
              }
              stream.write(shareCommitments.serialize());
              stream.write(additionalRpc);
            });

    return new OffChainInput(blindedBinaryShares, rpc);
  }

  /**
   * Off-chain input to a zk contract.
   *
   * @param blindedBinaryShares blinded shares to be sent to ZK nodes associated with the contract
   * @param rpc payload of the input to be sent to the contract binder. Contains commitments to the
   *     shares of the input and additional rpc
   */
  @SuppressWarnings("ArrayRecordComponent")
  public record OffChainInput(BlindedBinaryShares blindedBinaryShares, byte[] rpc) {}
}
