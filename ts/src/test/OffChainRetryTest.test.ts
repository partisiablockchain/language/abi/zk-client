/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import fetchMock from "jest-fetch-mock";
import { BitOutput } from "@secata-public/bitmanipulation-ts";
import { Client } from "../main";
import { OffChainInput, RealZkClient } from "../main";
// eslint-disable-next-line import/no-internal-modules
import * as sendSecretInputOffChainWithRetries from "./json-request-response/sendSecretInputOffChainWithRetries.json";
import {
  BlockchainTransactionClient,
  Chain,
  SenderAuthenticationKeyPair,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { CryptoUtils } from "../main";

jest.setTimeout(10000);

/** Testnet url. */
const TESTNET_URL = "https://node1.testnet.partisiablockchain.com";

/** Private key of the account interacting with the blockchain. */

const USER1_PRIVATE_KEY = "bb";

/** Address of the zk-contract. */
const ZK_CONTRACT = "039a61c7e8a5fe0e7354e15c8114c4f048d6e507c3";

/** Identifier of the secret input action in the zk contract. */
const SECRET_INPUT_INVOCATION = 0x40;

/** The secret input value. */
const SECRET_INPUT_VALUE = 10000;

describe("off chain retry", () => {
  let realZkClient: RealZkClient;
  let transactionSender: BlockchainTransactionClient;

  /** Create a RealZkClient by mocking the serialized contract. */
  beforeEach(async () => {
    fetchMock.resetMocks();

    transactionSender = BlockchainTransactionClient.create(
      TESTNET_URL,
      new SenderAuthenticationKeyPair(CryptoUtils.privateKeyToKeypair(USER1_PRIVATE_KEY))
    );

    const client = new Client(TESTNET_URL, 3);

    realZkClient = await RealZkClient.createForTest(ZK_CONTRACT, client, () =>
      Buffer.from("veryrandom")
    );

    fetchMock.resetMocks();
  });

  /** Nodes in the RealZkClient should be the same as the nodes in the mocked serialized contract. */
  test("get public keys of nodes", async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify({
        serializedContract:
          sendSecretInputOffChainWithRetries["11"].response.prettyEntity.serializedContract,
      })
    );

    const expectedKeys = sendSecretInputOffChainWithRetries[
      "11"
    ].response.prettyEntity.serializedContract.engines.engines.map((node) => node.publicKey);
    expect(
      (await realZkClient.getZkContractNodes())
        .getPublicKeysOfNodes()
        .map((key) => key.asBuffer().toString("base64"))
    ).toEqual(expectedKeys);
  });

  /** Send secret input off chain with retries. */
  test("send secret input off chain with retries", async () => {
    const serializedInput = BitOutput.serializeBits((out) =>
      out.writeSignedNumber(SECRET_INPUT_VALUE, 32)
    );
    const additionalRpc = Buffer.from([SECRET_INPUT_INVOCATION]);

    const offChainInput: OffChainInput = realZkClient.buildOffChainInputTransaction(
      serializedInput,
      additionalRpc
    );

    fetchMock.resetMocks();
    fetchMock.mockResponseOnce(
      JSON.stringify(sendSecretInputOffChainWithRetries["0"].response.prettyEntity)
    ); // nonce

    const chainId = sendSecretInputOffChainWithRetries["1"].response.prettyEntity.chainId;
    const chainResponse: Chain = {
      governanceVersion: 0,
      chainId,
      shards: [],
      plugins: {},
      features: [],
    };
    fetchMock.mockResponseOnce(JSON.stringify(chainResponse)); // chainId
    fetchMock.mockResponseOnce("{}"); // put transaction response

    const sentTransaction = await transactionSender.signAndSend(offChainInput.transaction, 100000);

    const txIdentifier = sentTransaction.transactionPointer.identifier;

    // Get nodes
    fetchMock.mockResponseOnce(
      JSON.stringify({
        serializedContract:
          sendSecretInputOffChainWithRetries["11"].response.prettyEntity.serializedContract,
      })
    );

    // The first 4 PUT requests will fail.
    fetchMock.mockRejectOnce(Error("Put failed"));
    fetchMock.mockRejectOnce(Error("Put failed"));
    fetchMock.mockRejectOnce(Error("Put failed"));
    fetchMock.mockRejectOnce(Error("Put failed"));

    await realZkClient.sendOffChainInputToNodes(
      ZK_CONTRACT,
      CryptoUtils.privateKeyToAccountAddress(USER1_PRIVATE_KEY),
      txIdentifier,
      offChainInput.blindedShares
    );

    const calls = fetchMock.mock.calls;

    // On-chain

    // GET request
    expect(calls["0"]["1"]?.method).toEqual(
      sendSecretInputOffChainWithRetries["0"].request.httpMethod
    );
    expect(calls["0"]["0"]).toEqual(
      `${TESTNET_URL}/chain/accounts/00db4eb445882bdb5c40c5959d1260d9383035b4e5`
    );

    // GET request
    expect(calls["1"]["1"]?.method).toEqual(
      sendSecretInputOffChainWithRetries["1"].request.httpMethod
    );
    expect(calls["1"]["0"]).toEqual(`${TESTNET_URL}/chain`);

    // PUT request
    expect(calls["2"]["1"]?.method).toEqual(
      sendSecretInputOffChainWithRetries["2"].request.httpMethod
    );
    expect(calls["2"]["0"]).toEqual(`${TESTNET_URL}/chain/transactions`);

    // GET request
    expect(calls["3"]["1"]?.method).toEqual(
      sendSecretInputOffChainWithRetries["11"].request.httpMethod.toString()
    );
    expect(calls["3"]["0"]).toContain(
      sendSecretInputOffChainWithRetries["11"].request.url.slice(0, 123).toString()
    );

    // Send secret off-chain to all 4 nodes
    // All URL are expected to be of the following form for X = 1,2,3,4 and with some random bytes at the end:
    // `https://nodeX.testnet.partisiablockchain.com/real/binary/input/${ZK_CONTRACT.asString()}/${USER1_ADDRESS.asString()}/RANDOMNESS`

    // All 4 PUT requests fail the first time
    for (let i = 1; i <= 4; i++) {
      const index = (11 + i).toString() as keyof typeof sendSecretInputOffChainWithRetries;
      expect(calls[3 + i]["1"]?.method).toEqual(
        sendSecretInputOffChainWithRetries[index].request.httpMethod
      );
      expect(calls[3 + i]["0"]).toContain(
        sendSecretInputOffChainWithRetries[index].request.url.slice(0, 122)
      );
    }

    // Resends 4 PUT requests
    for (let i = 1; i <= 4; i++) {
      const index = (11 + i).toString() as keyof typeof sendSecretInputOffChainWithRetries;
      expect(calls[7 + i]["1"]?.method).toEqual(
        sendSecretInputOffChainWithRetries[index].request.httpMethod
      );
      expect(calls[7 + i]["0"]).toContain(
        sendSecretInputOffChainWithRetries[index].request.url.slice(0, 122)
      );
    }
  });
});
