package com.partisiablockchain.language.zkclient.node;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.zkclient.ZkContractNode;

/** Stubbing out the http requests. */
public final class ZkContractNodeStub implements ZkContractNode {
  int numberOfFailuresBeforeSuccess;

  /**
   * Create new {@link ZkContractNodeStub}.
   *
   * @param numberOfFailuresBeforeSuccess number of times the stub will fail to send its request
   *     before succeeding
   */
  public ZkContractNodeStub(int numberOfFailuresBeforeSuccess) {
    this.numberOfFailuresBeforeSuccess = numberOfFailuresBeforeSuccess;
  }

  @Override
  public void sendShareToNode(BlockchainAddress account, Hash inputTransaction, byte[] share) {
    if (numberOfFailuresBeforeSuccess > 0) {
      numberOfFailuresBeforeSuccess--;
      throw new RuntimeException("Bad request");
    }
  }

  @Override
  public byte[] fetchShareFromNode(Signature signature, int variableId) {
    return new byte[0];
  }

  @Override
  public BlockchainPublicKey getPublicKey() {
    return null;
  }

  @Override
  public String getRestInterface() {
    return "";
  }

  @Override
  public Hash hashRequest(int variableId) {
    return null;
  }
}
