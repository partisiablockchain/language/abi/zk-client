/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { getRequest, putRequest } from "../../main/util/Api";
import fetchMock from "jest-fetch-mock";

const url = "http://not-a-real-url";
const response = "test-response";

describe("Api tests", () => {
  test("get transaction status 404", async () => {
    fetchMock.mockResponseOnce(JSON.stringify({ data: "test" }), { status: 404 });
    const get: boolean | undefined = await getRequest<boolean>("http://not-a-real-url");
    expect(get).toBeUndefined();
  });

  test("put transaction", async () => {
    const put: boolean | undefined = await putRequest<boolean>("http://not-a-real-url", false);
    expect(put).toBeTruthy();
  });

  test("get request headers", async () => {
    fetchMock.resetMocks();
    fetchMock.mockOnce(response);

    getRequest<string>(url);

    expect(fetchMock.mock.calls[0]["1"]).toEqual({
      body: null,
      headers: {
        Accept: "application/json, text/plain, */*",
      },
      method: "GET",
    });
  });

  test("put request headers", async () => {
    fetchMock.resetMocks();
    fetchMock.mockOnce(response);

    putRequest<boolean>(url, false);

    expect(fetchMock.mock.calls[0]["1"]).toEqual({
      method: "PUT",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: "false",
    });
  });
});
